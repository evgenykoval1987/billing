<?php

namespace Laraspace\Models;

use Illuminate\Database\Eloquent\Model;
use Laraspace\User;
use Illuminate\Database\Eloquent\Builder;
use Auth;

class Notification extends Model
{    
    protected static function boot() {
        parent::boot();
        static::addGlobalScope('wh', function (Builder $builder) {
            $user_id = Auth::user()->getId();

            $builder->where('user_id', $user_id);
        });

        static::addGlobalScope('order', function (Builder $builder) {
	        $builder->orderBy('created_at', 'desc');
	    });
    }

	public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
