<?php

namespace Laraspace\Models;

use Illuminate\Database\Eloquent\Model;
use Laraspace\Models\Status;
use Illuminate\Database\Eloquent\Builder;
use Laraspace\Models\Project;
use Laraspace\Models\TaskUsers;
use Laraspace\User;
use Auth;

class Task extends Model
{
	protected static function boot() {
        parent::boot();
        static::addGlobalScope('order', function (Builder $builder) {
            $role_id = Auth::user()->getRole();
            $user_id = Auth::user()->getId();

           if ($role_id == 3){
                $builder->whereIn('id', function($query){
                    $query->select('task_id')
                    ->from(with(new TaskUsers)->getTable())
                    ->where('user_id', Auth::user()->id);
                });
            }
        });
    }

    public function status(){
        return $this->belongsTo(Status::class, 'status_id', 'id');
    }

    public function project(){
        return $this->belongsTo(Project::class, 'project_id', 'id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function task_users(){
        return $this->hasMany(TaskUsers::class, 'task_id', 'id');
    }
}
