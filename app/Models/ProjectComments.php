<?php

namespace Laraspace\Models;

use Illuminate\Database\Eloquent\Model;
use Laraspace\Models\Project;
use Illuminate\Database\Eloquent\Builder;
use Laraspace\User;

class ProjectComments extends Model
{
    protected $table = 'project_comments';

    protected static function boot() {
	    parent::boot();
	    static::addGlobalScope('order', function (Builder $builder) {
	        $builder->orderBy('created_at', 'desc');
	    });
	}

    public function project(){
        return $this->belongsTo(Project::class, 'project_id', 'id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
