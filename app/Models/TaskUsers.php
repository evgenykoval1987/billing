<?php

namespace Laraspace\Models;

use Illuminate\Database\Eloquent\Model;
use Laraspace\User;
use Laraspace\Task;

class TaskUsers extends Model
{
    public $timestamps = false;
    protected $table = 'task_users';

    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function task(){
        return $this->belongsTo(Task::class, 'task_id', 'id');
    }
}
