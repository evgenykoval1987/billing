<?php

namespace Laraspace\Models;

use Illuminate\Database\Eloquent\Model;
use Laraspace\Models\Status;

class Evento extends Model
{    
	protected $table = 'eventos';

	public function status(){
        return $this->belongsTo(Status::class, 'status_id', 'id');
    }
}
