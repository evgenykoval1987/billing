<?php

namespace Laraspace\Models;

use Illuminate\Database\Eloquent\Model;
use Laraspace\Models\Status;
use Laraspace\Models\ProjectUsers;
use Laraspace\Models\ProjectComments;
use Laraspace\User;
use Illuminate\Database\Eloquent\Builder;
use Auth;

class Project extends Model
{
	protected static function boot() {
	    parent::boot();
	    static::addGlobalScope('order', function (Builder $builder) {
	        $builder->orderBy('created_at', 'desc');

            $role_id = Auth::user()->getRole();
            $user_id = Auth::user()->getId();

            if ($role_id == 2){
                $builder->where('manager_id', $user_id);
            }
            else if ($role_id == 4){
                $builder->where('client_id', $user_id);
            }
            else if ($role_id == 3){
                $builder->whereIn('id', function($query){
                    $query->select('project_id')
                    ->from(with(new ProjectUsers)->getTable())
                    ->where('user_id', Auth::user()->id);
                });
            }
	    });
	}

    public function client(){
        return $this->belongsTo(User::class, 'client_id', 'id');
    }

    public function manager(){
        return $this->belongsTo(User::class, 'manager_id', 'id');
    }

    public function status(){
        return $this->belongsTo(Status::class, 'status_id', 'id');
    }

    public function project_users(){
        return $this->hasMany(ProjectUsers::class, 'project_id', 'id');
    }

    public function comments(){
        return $this->hasMany(ProjectComments::class, 'project_id', 'id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
