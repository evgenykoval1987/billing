<?php

namespace Laraspace\Models;

use Illuminate\Database\Eloquent\Model;
use Laraspace\User;

class ProjectUsers extends Model
{
    public $timestamps = false;
    protected $table = 'project_users';

    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
