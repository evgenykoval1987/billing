<?php

namespace Laraspace\Models;

use Illuminate\Database\Eloquent\Model;
use Laraspace\Models\Task;
use Illuminate\Database\Eloquent\Builder;
use Laraspace\User;

class TaskComments extends Model
{
    protected $table = 'task_comments';

    protected static function boot() {
	    parent::boot();
	    static::addGlobalScope('order', function (Builder $builder) {
	        $builder->orderBy('created_at', 'desc');
	    });
	}

    public function task(){
        return $this->belongsTo(Task::class, 'task_id', 'id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
