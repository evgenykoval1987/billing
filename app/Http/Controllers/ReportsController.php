<?php

namespace Laraspace\Http\Controllers;

use Illuminate\Http\Request;
use Laraspace\Models\Task;
use Laraspace\Models\TaskUsers;
use Laraspace\Models\TaskComments;
use Laraspace\Models\Status;
use Laraspace\Models\Project;
use Laraspace\User;
use Auth;

class ReportsController extends Controller
{
	public function user(Request $request, $id){
		$user = User::find($id);
		if ($user->role_id == 2){
			$stat = [];
			$stat['projects_in_work'] = Project::where('manager_id',$id)->where('status_id', 2)->get()->count();
			$tasks_in_work = 0;
			$projects = Project::where('manager_id',$id)->get();
			foreach ($projects as $key => $project) {
				$tasks = Task::where('project_id', $project->id)->where('status_id',9)->get()->count();
				$tasks_in_work += $tasks;
			}
			$stat['tasks_in_work'] = $tasks_in_work;

			$clients_in_work = [];
			$projects = Project::where('manager_id',$id)->where('status_id', 2)->get();
			foreach ($projects as $key => $project) {
				if(!in_array($project->client_id, $clients_in_work)){
					$clients_in_work[] = $project->client_id;
				}
			}
			$stat['clients_in_work'] = count($clients_in_work);

			$tasks_in_end = [];
			$projects = Project::where('manager_id',$id)->get();
			foreach ($projects as $key => $project) {
				$tasks = Task::where('project_id', $project->id)->where('status_id','!=',16)->where('date_end', '<', now())->get();
				foreach ($tasks as $key => $task) {
					$tasks_in_end[] = $task;
				}
			}
			$stat['tasks_in_end'] = $tasks_in_end;

			$stat['projects_in_end'] = Project::where('manager_id',$id)->where('status_id', 2)->where('date_end', '<', now())->get();

			return view('admin.reports.manager')->with(['user' => $user, 'stat' => $stat]);
		}
		else if($user->role_id == 3){
			$stat = [];

			$tasks_in_work = 0;
			$tasks = Task::where('status_id',9)->get();
			foreach ($tasks as $key => $task) {
				$task_users = $task->task_users()->where('user_id',$id)->get()->count();
				if ($task_users > 0)
					$tasks_in_work++;
			}
			$stat['tasks_in_work'] = $tasks_in_work;


			$tasks_in_end = 0;
			$tasks = Task::where('status_id',9)->where('date_end', '<', now())->get();
			foreach ($tasks as $key => $task) {
				$task_users = $task->task_users()->where('user_id',$id)->get()->count();
				if ($task_users > 0)
					$tasks_in_end++;
			}
				
			$stat['tasks_in_end'] = $tasks_in_end;


			return view('admin.reports.worker')->with(['user' => $user, 'stat' => $stat]);
		}
	}
}