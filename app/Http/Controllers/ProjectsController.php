<?php

namespace Laraspace\Http\Controllers;

use Illuminate\Http\Request;
use Laraspace\User;
use Laraspace\Models\Status;
use Laraspace\Models\Project;
use Laraspace\Models\ProjectUsers;
use Laraspace\Models\ProjectComments;
use Laraspace\Models\Notification;
use Auth;

class ProjectsController extends Controller
{
    public function index()
    {
        $add = true;
        $role_id = Auth::user()->getRole(); 
        if ($role_id == 3)
            $add = false;

        return view('admin.projects.index')->with(['add' => $add]);
    }

    public function addForm(){
    	$users = User::get();
    	$statuses = Status::get();
    	$h1 = 'Создание проекта';
    	$action = route('admin.projects.add');

        $client_id = false;
        $role_id = Auth::user()->getRole(); 
        if ($role_id == 4){
            $client_id = Auth::user()->getId();
        }

    	return view('admin.projects.add')->with(['users' => $users, 'statuses' => $statuses, 'action' => $action, 'h1' => $h1, 'client_id' => $client_id]);
    }

    public function addProject(Request $request){
    	$project = new Project();
    	$project->client_id = $request->client_id;
    	$project->title = $request->name;
    	$project->manager_id = $request->manager_id;
    	$project->status_id = $request->status_id;
    	$project->comment = $request->comment;
    	$project->date_start = date('Y-m-d', strtotime($request->date_start));
    	$project->date_end = date('Y-m-d', strtotime($request->date_end));
        $project->user_id = Auth::user()->getID();
    	$project->save();

    	$project_id = $project->id;

        if ($request->workers){
        	foreach ($request->workers as $key => $worker) {
        		$project_users = new ProjectUsers();
        		$project_users->project_id = $project_id;
        		$project_users->user_id = $worker;
        		$project_users->save();
        	}
        }

        $notification = new Notification();
        $notification->text = 'У вас появился новый проект от клиента. <a href='.route('admin.projects.show', $project_id).'>'.$request->name.'</a>';
        $notification->user_id = $request->manager_id;
        $notification->save();

    	$request->session()->put('project_added', 'Проект '.$request->name.' добавлен');

    	return redirect(route('admin.projects'));
    }

    public function ajax(){
        $data = [];
        $projects = Project::get();
        $role_id = Auth::user()->getRole();

        foreach ($projects as $key => $project) {
            $actions = [];
            $actions[] = '<a href="'.route('admin.projects.show',$project->id).'" class="btn btn-default btn-sm"><i class="icon-fa icon-fa-search"></i></a>';
            if ($role_id != 3)
                $actions[] = '<a href="'.route('admin.projects.edit',$project->id).'" class="btn btn-default btn-sm"><i class="icon-fa icon-fa-pencil"></i></a>';
            if ($role_id == 1)
                $actions[] = '<a href="'.route('admin.projects.destroy',$project->id).'" class="btn btn-default btn-sm" data-token="'.csrf_token().'" data-delete data-confirmation="notie"><i class="icon-fa icon-fa-trash"></i></a>';


            $data[] = [
                $project->title,
                $project->client->name,
                date('d.m.Y',strtotime($project->date_start)).' - '.date('d.m.Y',strtotime($project->date_end)),
                $project->status->title,
                implode(' ', $actions)
            ];
        }

        return response()->json(['data' => $data]);
    }

    public function destroy($id){
        $project = Project::findOrFail($id);
        $project->delete();
        flash()->success('Проект удален');

        return redirect()->back();
    }

    public function editForm($id){
    	$users = User::get();
    	$statuses = Status::get();
    	$info = Project::findOrFail($id);
    	$info->date_start = date('d.m.Y', strtotime($info->date_start));
    	$info->date_end = date('d.m.Y', strtotime($info->date_end));
    	$action = route('admin.projects.edit-project', $id);
    	$h1 = 'Редактирование проекта';
    	$workers = [];
    	$results = $info->project_users()->get();
    	foreach ($results as $key => $result) {
    		$workers[] = $result->user_id;
    	}

        $client_id = false;
        $role_id = Auth::user()->getRole(); 
        if ($role_id == 4){
            $client_id = Auth::user()->getId();
        }

    	return view('admin.projects.add')->with(['users' => $users, 'statuses' => $statuses, 'info' => $info, 'action' => $action, 'h1' => $h1, 'workers' => $workers, 'client_id' => $client_id]);
    }

    public function edit(Request $request, $id){
    	$project = Project::findOrFail($id);

        $old_status_id = $project->status_id;

    	$project->client_id = $request->client_id;
    	$project->title = $request->name;
    	$project->manager_id = $request->manager_id;
    	$project->status_id = $request->status_id;
    	$project->comment = $request->comment;
    	$project->date_start = date('Y-m-d', strtotime($request->date_start));
    	$project->date_end = date('Y-m-d', strtotime($request->date_end));
    	$project->save();

    	$project_id = $id;

        $old_status = Status::find($old_status_id)->title;
        $new_status = Status::find($request->status_id)->title;

    	ProjectUsers::where('project_id', $id)->delete();
        if ($request->workers){
        	foreach ($request->workers as $key => $worker) {
        		$project_users = new ProjectUsers();
        		$project_users->project_id = $project_id;
        		$project_users->user_id = $worker;
        		$project_users->save();
        	}
        }

        if ($old_status_id != $request->status_id){
            $notification = new Notification();
            $notification->text = 'Статус проекта <a href='.route('admin.projects.show', $project_id).'>'.$project->title.'</a> изменен c '.$old_status.' на '.$new_status;
            $notification->user_id = $project->manager_id;
            $notification->save();
        }

    	$request->session()->put('project_added', 'Проект '.$request->name.' изминен');

    	return redirect(route('admin.projects'));
    }

    public function show($id){
    	$info = Project::findOrFail($id);
    	//$info->comment = htmlentities($info->comment);

    	$statuses = Status::get();

    	$workers = $info->project_users()->get();
    	//dd($workers);

        $add = true;
        $role_id = Auth::user()->getRole(); 
        if ($role_id == 3)
            $add = false;

    	return view('admin.projects.show')->with(['info' => $info, 'statuses' => $statuses, 'workers' => $workers, 'add' => $add]);
    }

    public function addComment(Request $request){
    	$project_id = $request->project_id;
    	$comment = $request->comment;

    	$pc = new ProjectComments();
    	$pc->text = $comment;
    	$pc->project_id = $project_id;
    	$pc->user_id = Auth::user()->getID();
    	$pc->save();

    	return response()->json(true);
    }

    public function getComments($project_id){
    	$comments = ProjectComments::where('project_id', $project_id)->get();

    	return view('admin.projects.comments')->with(['comments' => $comments]);
    }

    public function ajaxInWork(){
        $data = [];
        $projects = Project::where('status_id',2)->get();
        $role_id = Auth::user()->getRole();

        foreach ($projects as $key => $project) {
            $actions = [];
            $actions[] = '<a href="'.route('admin.projects.show',$project->id).'" class="btn btn-default btn-sm"><i class="icon-fa icon-fa-search"></i></a>';
            /*if ($role_id != 3)
                $actions[] = '<a href="'.route('admin.projects.edit',$project->id).'" class="btn btn-default btn-sm"><i class="icon-fa icon-fa-pencil"></i></a>';
            if ($role_id == 1)
                $actions[] = '<a href="'.route('admin.projects.destroy',$project->id).'" class="btn btn-default btn-sm" data-token="'.csrf_token().'" data-delete data-confirmation="notie"><i class="icon-fa icon-fa-trash"></i></a>';*/


            $data[] = [
                $project->title,
                $project->client->name,
                $project->manager->name,
                date('d.m.Y',strtotime($project->date_start)).' - '.date('d.m.Y',strtotime($project->date_end)),
                $project->status->title,
                implode(' ', $actions)
            ];
        }

        return response()->json(['data' => $data]);
    }
}
