<?php

namespace Laraspace\Http\Controllers;

use Illuminate\Http\Request;
use Laraspace\Models\Task;
use Laraspace\Models\TaskUsers;
use Laraspace\Models\TaskComments;
use Laraspace\Models\Status;
use Laraspace\Models\Project;
use Laraspace\Models\Evento;
use Auth;

class CalendarController extends Controller
{
	public function index()
    {
    	$dots = [];

    	$role_id = Auth::user()->getRole();
    	if ($role_id != 3 && $role_id != 4){
    		$dots[] = [
    			'title' => 'Окончание проекта',
    			'label' => 'fc-event-dot fc-event-danger-dot'
    		];
    	}

    	$dots[] = [
			'title' => 'Окончание задачи',
			'label' => 'fc-event-dot fc-event-warning-dot'
		];

		$dots[] = [
			'title' => 'Мои события',
			'label' => 'fc-event-dot fc-event-primary-dot'
		];

		$statuses = Status::where('status_group_id',3)->get();

        return view('admin.calendar.index')->with(['dots' => $dots, 'statuses' => $statuses]);
    }

    public function ajax(){
    	$events = [];

    	$role_id = Auth::user()->getRole();
    	$user_id = Auth::user()->getID();
    	if ($role_id == 1){
    		$projects = Project::all();
    		foreach ($projects as $key => $project) {
    			$events[] = [
		    		'title' => $project->title,
		          	'start' => $project->date_end,
		          	'constraint' => 'allDayEvent',
		          	'color' => '#f35a3d',
		          	'url' => route('admin.projects.show',$project->id)
		    	];
    		}

    		$tasks = Task::where('status_id','!=','16')->get();
	    	foreach ($tasks as $key => $task) {
				$events[] = [
		    		'title' => $task->title,
		          	'start' => $task->date_end,
		          	'constraint' => 'allDayEvent',
		          	'color' => '#f0ad4e',
		          	'url' => route('admin.projects.show',$task->project_id)
		    	];
			}
    	}
    	if ($role_id == 2){
    		$projects = Project::where('manager_id', $user_id)->get();
    		foreach ($projects as $key => $project) {
    			$events[] = [
		    		'title' => $project->title,
		          	'start' => $project->date_end,
		          	'constraint' => 'allDayEvent',
		          	'color' => '#f35a3d',
		          	'url' => route('admin.projects.show',$project->id)
		    	];
    		}

    		$tasks = Task::where('status_id','!=','16')->get();
	    	foreach ($tasks as $key => $task) {
	    		if ($task->project->manager_id != $user_id)
	    			continue;
				$events[] = [
		    		'title' => $task->title,
		          	'start' => $task->date_end,
		          	'constraint' => 'allDayEvent',
		          	'color' => '#f0ad4e',
		          	'url' => route('admin.projects.show',$task->project_id)
		    	];
			}
    	}
    	if ($role_id == 4){
    		/*$projects = Project::where('client_id', $user_id)->get();
    		foreach ($projects as $key => $project) {
    			$events[] = [
		    		'title' => $project->title,
		          	'start' => $project->date_end,
		          	'constraint' => 'allDayEvent',
		          	'color' => '#f35a3d',
		          	'url' => route('admin.projects.show',$project->id)
		    	];
    		}*/

    		$tasks = Task::where('status_id','!=','16')->get();
	    	foreach ($tasks as $key => $task) {
	    		if ($task->project->client_id != $user_id)
	    			continue;
				$events[] = [
		    		'title' => $task->title,
		          	'start' => $task->date_end,
		          	'constraint' => 'allDayEvent',
		          	'color' => '#f0ad4e',
		          	'url' => route('admin.projects.show',$task->project_id)
		    	];
			}
    	}

    	if ($role_id == 3){
    		$tasks = TaskUsers::where('user_id',$user_id)->get();
	    	foreach ($tasks as $key => $task) {
				$events[] = [
		    		'title' => $task->task->title,
		          	'start' => $task->task->date_end,
		          	'constraint' => 'allDayEvent',
		          	'color' => '#f0ad4e',
		          	'url' => route('admin.projects.show',$task->task->project_id)
		    	];
			}
    	}

    	$eventos = Evento::where('user_id',Auth::user()->getID())->get();
    	foreach ($eventos as $key => $event) {
			$events[] = [
	    		'title' => $event->title,
	          	'start' => $event->dt,
	          	'dt' => date('d.m.Y', strtotime($event->dt)),
	          	'description' => $event->description,
	          	'status' => $event->status->title,
	          	'constraint' => 'allDayEvent',
	          	'color' => '#007dcc',
	          	'popup' => true
	    	];
		}
    	
    	return response()->json($events);
    }

    public function add(Request $request){
    	$event = new Evento();
    	$event->title = $request->title;
    	$event->description = $request->description;
    	$event->status_id = $request->status_id;
    	$event->dt = date('Y-m-d', strtotime($request->dt));
    	$event->user_id = Auth::user()->getID();
    	$event->save();

    	echo 1;
    }
}