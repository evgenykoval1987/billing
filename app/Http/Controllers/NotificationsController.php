<?php

namespace Laraspace\Http\Controllers;

use Illuminate\Http\Request;
use Laraspace\Models\Task;
use Laraspace\Models\TaskUsers;
use Laraspace\Models\TaskComments;
use Laraspace\Models\Status;
use Laraspace\Models\Project;
use Laraspace\Models\Notification;
use Laraspace\Models\Evento;
use Laraspace\User;
use Auth;

class NotificationsController extends Controller
{
	public function index(){
		$notifications = Notification::all();
		
		return view('admin.notifications.list')->with(['notifications' => $notifications]);
	}

	public function clear(){
		$notifications = Notification::all();
		foreach ($notifications as $key => $notification) {
			$notification->delete();
		}

		return redirect(route('admin.notifications'));
	}

	public function check(){
		$notifications = Notification::all();
		foreach ($notifications as $key => $notification) {
			$notification->status = 1;
			$notification->save();
		}

		echo 1;
	}

	public function cron(){
		//За два дня до окончания срока выполнения задачи
		$tasks = Task::where('date_end', date('Y-m-d',strtotime('+ 2 day')))->get();
		foreach ($tasks as $key => $task) {
			$notification = new Notification();
			$notification->text = 'Срок выполнения задачи <a href='.route('admin.tasks.show', $task->id).'>'.$task->title.'</a> подходит к концу. Постарайтесь завершить задачу вовремя';
			$notification->user_id = $task->project->manager_id;
			$notification->save();

			foreach ($task->task_users()->get() as $key => $user) {
				$notification = new Notification();
				$notification->text = 'Срок выполнения задачи <a href='.route('admin.tasks.show', $task->id).'>'.$task->title.'</a> подходит к концу. Постарайтесь завершить задачу вовремя';
				$notification->user_id = $user->user_id;
				$notification->save();
			}
		}

		//В день окончания срока выполнения задачи
		$tasks = Task::where('date_end', date('Y-m-d'))->where('status_id','!=',16)->get();
		foreach ($tasks as $key => $task) {
			$notification = new Notification();
			$notification->text = 'Сегодня нужно отправить задачу <a href='.route('admin.tasks.show', $task->id).'>'.$task->title.'</a> на проверку менеджеру';
			$notification->user_id = $task->project->manager_id;
			$notification->save();
			
			foreach ($task->task_users()->get() as $key => $user) {
				$notification = new Notification();
				$notification->text = 'Сегодня нужно отправить задачу <a href='.route('admin.tasks.show', $task->id).'>'.$task->title.'</a> на проверку менеджеру';
				$notification->user_id = $user->user_id;
				$notification->save();
			}
		}

		//За неделю до окончания срока реализации проекта
		$projects = Project::where('date_end', date('Y-m-d',strtotime('+ 7 day')))->get();
		foreach ($projects as $key => $project) {
			$notification = new Notification();
			$notification->text = 'Осталась неделя, чтобы закрыть все задачи по проекту <a href='.route('admin.projects.show', $project->id).'>'.$project->title.'</a>. Соберите рабочую встречу, чтобы быть уверенными что проект завершится вовремя ';
			$notification->user_id = $project->manager_id;
			$notification->save();

			$users = User::where('role_id',1)->get();
			foreach ($users as $key => $user) {
				$notification = new Notification();
				$notification->text = 'Осталась неделя, чтобы закрыть все задачи по проекту <a href='.route('admin.projects.show', $project->id).'>'.$project->title.'</a>. Соберите рабочую встречу, чтобы быть уверенными что проект завершится вовремя ';
				$notification->user_id = $user->id;
				$notification->save();
			}
		}

		//В день окончания реализации проекта
		$projects = Project::where('date_end', date('Y-m-d'))->get();
		foreach ($projects as $key => $project) {
			$notification = new Notification();
			$notification->text = 'Сегодня нужно передать проект <a href='.route('admin.projects.show', $project->id).'>'.$project->title.'</a> на проверку клиенту';
			$notification->user_id = $project->manager_id;
			$notification->save();

			$users = User::where('role_id',1)->get();
			foreach ($users as $key => $user) {
				$notification = new Notification();
				$notification->text = 'Сегодня нужно передать проект <a href='.route('admin.projects.show', $project->id).'>'.$project->title.'</a> на проверку клиенту';
				$notification->user_id = $user->id;
				$notification->save();
			}
		}

		$events = Evento::where('dt',date('Y-m-d'))->get();
		foreach ($events as $key => $event) {
			$notification = new Notification();
			$notification->text = 'У вас событие '.$event->status->title.' '.date('d.m.Y', strtotime($event->dt));
			$notification->user_id = $event->user_id;
			$notification->save();
		}

		$events = Evento::where('dt',date('Y-m-d',strtotime('+ 1 day')))->get();
		foreach ($events as $key => $event) {
			$notification = new Notification();
			$notification->text = 'Завтра у вас назначено событие '.$event->status->title;
			$notification->user_id = $event->user_id;
			$notification->save();
		}
	}
}