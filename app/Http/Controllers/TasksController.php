<?php

namespace Laraspace\Http\Controllers;

use Illuminate\Http\Request;
use Laraspace\Models\Task;
use Laraspace\Models\TaskUsers;
use Laraspace\Models\TaskComments;
use Laraspace\Models\Status;
use Laraspace\Models\Project;
use Laraspace\Models\Notification;
use Auth;

class TasksController extends Controller
{
    public function addTask(Request $request){
    	$task = new Task();
    	$task->title = $request->name;
    	$task->description = $request->description;
    	$task->project_id = $request->project_id;
    	$task->status_id = $request->status_id;
    	$task->date_start = date('Y-m-d', strtotime($request->date_start));
    	$task->date_end = ($request->date_end != '') ? date('Y-m-d', strtotime($request->date_end)) : null;
        $task->user_id = Auth::user()->getID();
    	$task->save();

    	$task_id = $task->id;

    	foreach ($request->workers as $key => $worker) {
    		$task_users = new TaskUsers();
    		$task_users->task_id = $task_id;
    		$task_users->user_id = $worker;
    		$task_users->save();

            $notification = new Notification();
            $notification->text = 'У вас появилась новая задача от менеджера. <a href='.route('admin.tasks.show', $task_id).'>'.$task->title.'</a>';
            $notification->user_id = $worker;
            $notification->save();
    	}

    	return response()->json(true);
    }

    public function ajax(Request $request){
    	$project_id = $request->project_id;
        $data = [];
        $tasks = Task::where('project_id', $project_id)->get();
        $role_id = Auth::user()->getRole();

        foreach ($tasks as $key => $task) {
            $actions = [];
            $actions[] = '<a href="'.route('admin.tasks.show',$task->id).'" class="btn btn-default btn-sm"><i class="icon-fa icon-fa-search"></i></a>';
            if ($role_id != 3){
                $actions[] = '<a href="'.route('admin.tasks.edit',$task->id).'" class="btn btn-default btn-sm" data-edit><i class="icon-fa icon-fa-pencil"></i></a>';
                $actions[] = '<a href="'.route('admin.tasks.destroy',$task->id).'" class="btn btn-default btn-sm" data-token="'.csrf_token().'" data-delete data-confirmation="notie"><i class="icon-fa icon-fa-trash"></i></a>';
            }

            $data[] = [
            	date('d.m.Y H:i',strtotime($task->updated_at)),
                $task->title,
                date('d.m.Y',strtotime($task->date_start)).(($task->date_end != '') ? ' - '.date('d.m.Y',strtotime($task->date_end)) : ' - безсрочно'),
                $task->status->title,
                implode(' ', $actions)
            ];
        }

        return response()->json(['data' => $data]);
    }

    public function ajaxInWork(){
        $data = [];
        $tasks = Task::where('status_id', 9)->get();

        foreach ($tasks as $key => $task) {
            $items = [];
            $workers = $task->task_users()->get();
            foreach ($workers as $key => $worker) {
                $items[] = $worker->user->name;
            }
            $workers = implode('<br>', $items);
            $data[] = [
                $task->title,
                $task->project->title,
                $task->project->client->name,
                $task->project->manager->name,
                $workers,
                date('d.m.Y',strtotime($task->date_start)).' - '.date('d.m.Y',strtotime($task->date_end)),
                $task->status->title
            ];
        }

        return response()->json(['data' => $data]);
    }

    public function destroy($id){
        $task = Task::findOrFail($id);
        $task->delete();
        flash()->success('Задача удалена');

        return redirect()->back();
    }

    public function editForm($id){
    	$info = Task::find($id);
        $statuses = Status::get();
    	$workers = $info->project->project_users()->get();
    	$workers_in = [];
    	foreach ($info->task_users()->get() as $in){
    		$workers_in[] = $in->user_id;
    	}

        return view('admin.tasks.edit')->with(['info' => $info, 'statuses' => $statuses, 'workers' => $workers, 'workers_in' => $workers_in]);
    }

    public function edit(Request $request, $id){
        $task = Task::findOrFail($id);

        $old_status_id = $task->status_id;

    	$task->title = $request->name;
    	$task->description = $request->description;
    	$task->status_id = $request->status_id;
    	$task->date_start = date('Y-m-d', strtotime($request->date_start));
    	$task->date_end = ($request->date_end != '') ? date('Y-m-d', strtotime($request->date_end)) : null;
    	$task->save();

    	$task_id = $id;

        $old_status = Status::find($old_status_id)->title;
        $new_status = Status::find($request->status_id)->title;

    	TaskUsers::where('task_id', $id)->delete();

    	foreach ($request->workers as $key => $worker) {
    		$task_users = new TaskUsers();
    		$task_users->task_id = $task_id;
    		$task_users->user_id = $worker;
    		$task_users->save();

            if ($old_status_id != $request->status_id){
                $notification = new Notification();
                $notification->text = 'Статус задачи <a href='.route('admin.tasks.show', $task_id).'>'.$task->title.'</a> изменен c '.$old_status.' на '.$new_status;
                $notification->user_id = $worker;
                $notification->save();
            }
    	}

        if ($old_status_id != $request->status_id){
            $notification = new Notification();
            $notification->text = 'Статус задачи <a href='.route('admin.tasks.show', $task_id).'>'.$task->title.'</a> изменен c '.$old_status.' на '.$new_status;
            $notification->user_id = $task->project->manager_id;
            $notification->save();
        }

    	return response()->json(true);
    }

    public function show($id){ 
    	$info = Task::findOrFail($id);
        $project_info = Project::findOrFail($info->project_id);

        $workers = $info->task_users()->get();

        return view('admin.tasks.show')->with(['info' => $info, 'project_info' => $project_info, 'workers' => $workers]);
    }

    public function addComment(Request $request){
        $task_id = $request->task_id;
        $comment = $request->comment;

        $pc = new TaskComments();
        $pc->text = $comment;
        $pc->task_id = $task_id;
        $pc->user_id = Auth::user()->getID();
        $pc->save();

        return response()->json(true);
    }

    public function getComments($task_id){
        $comments = TaskComments::where('task_id', $task_id)->get();

        return view('admin.tasks.comments')->with(['comments' => $comments]);
    }
}
