<?php
namespace Laraspace\Http\Controllers;

use Laraspace\User;
use Laraspace\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;

class AllreportsController extends Controller
{
	public function projects(){
		return view('admin.allreports.projects')->with([]);
	}

	public function tasks(){
		return view('admin.allreports.tasks')->with([]);
	}
}