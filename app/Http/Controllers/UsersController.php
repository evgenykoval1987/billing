<?php
namespace Laraspace\Http\Controllers;

use Laraspace\User;
use Laraspace\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::get();
        $roles = Role::get();

        return view('admin.users.index')->with(['users' => $users, 'roles' => $roles]);
    }

    public function show($id)
    {
        $user = User::findOrFail($id);

        return view('admin.users.show')->with('user', $user);
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        flash()->success('User Deleted');

        return redirect()->back();
    }

    public function lock($id){
        $user = User::findOrFail($id);
        $user->lock = 1;
        $user->save();

    }

    public function unlock($id){
        $user = User::findOrFail($id);
        $user->lock = 0;
        $user->save();
    }

    public function ajax(){ 
        $data = [];
        $users = User::get();

        foreach ($users as $key => $user) {
            $actions = [];
            $actions[] = '<a href="'.route('admin.users.get',$user->id).'" class="btn btn-default btn-sm" data-edit><i class="icon-fa icon-fa-pencil"></i></a>';
            $actions[] = '<a href="'.route('users.show',$user->id).'" class="btn btn-default btn-sm"><i class="icon-fa icon-fa-search"></i></a>';
            $actions[] = '<a href="'.route('users.destroy',$user->id).'" class="btn btn-default btn-sm" data-token="'.csrf_token().'" data-delete data-confirmation="notie"><i class="icon-fa icon-fa-trash"></i></a>';
            if($user->lock)
                $actions[] = '<a href="'.route('admin.users.unlock',$user->id).'" class="btn btn-default btn-sm" data-unlock data-token="'.csrf_token().'"><i class="icon-fa icon-fa-lock" ></i></a>';
            else
                $actions[] = '<a href="'.route('admin.users.lock',$user->id).'" class="btn btn-default btn-sm" data-lock data-token="'.csrf_token().'"><i class="icon-fa icon-fa-unlock" ></i></a>';

            if ($user->role_id == 2 || $user->role_id == 3){
                $actions[] = '<a href="'.route('admin.reports.user',$user->id).'" class="btn btn-default btn-sm" title="Отчет"><i class="icon-fa icon-fa-bar-chart" ></i></a>';
            }


            $data[] = [
                $user->name,
                $user->email,
                $user->role->title,
                date('d.m.Y H:i',strtotime($user->created_at)),
                implode(' ', $actions)
            ];
        }

        return response()->json(['data' => $data]);
    }

    public function add(Request $request){
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->role_id = $request->role_id;
        $user->company = $request->company;
        $user->password = Hash::make($request->password);
        $user->save();

        return response()->json(true);
    }

    public function checkEmail(Request $request){
        $user = User::where('email',$request->email)->first();
        if ($user)
            return response()->json(false);
        else
            return response()->json(true);
    }

    public function get($id){
        $user = User::find($id);
        $roles = Role::get();

        return view('admin.users.edit')->with(['user' => $user, 'roles' => $roles]);
    }

    public function edit(Request $request, $id){
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->role_id = $request->role_id;
        $user->company = $request->company;
        if ($request->password != '')
            $user->password = Hash::make($request->password);
        $user->save();

        return response()->json(true);
    }

    public function iam(){
        $id = Auth::user()->getId();
        $user = User::findOrFail($id);

        return view('admin.users.iam')->with(['user' => $user]);
    }

    public function iamEdit(Request $request){
        $id = Auth::user()->getId();
        $user = User::find($id);
        $user->name = $request->name;
        $user->company = $request->company;
        if ($request->password != '')
            $user->password = Hash::make($request->password);
        $user->save();

        return response()->json(true);
    }
}
