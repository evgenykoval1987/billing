<?php

namespace Laraspace\Http\Middleware;
use Auth;
use Closure;

class NotWorker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->getRole() == '3') {
            return redirect()->to('admin/404');
        }

        return $next($request);
    }
}
