<?php
namespace Laraspace\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Auth;
use Laraspace\User;
use Laraspace\Models\Menu;
use Laraspace\Models\Project;
use Laraspace\Models\ProjectUsers;
use Laraspace\Models\Task;
use Laraspace\Models\TaskUsers;
use Laraspace\Models\Notification;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        
        View::composer('admin.layouts.partials.nav.menu-horizontal', function($view) {
            $role_id = Auth::user()->getRole();
            $menuitems = [];
            $items = Menu::where('role_id',$role_id)->where('parent_id',0)->get();
            foreach ($items as $key => $item) {
                $children = [];

                $subitems = Menu::where('role_id',$role_id)->where('parent_id',$item->id)->get();
                foreach ($subitems as $ke => $subitem) {
                    $children[] = [
                        'title' => $subitem->title,
                        'link' => $subitem->link,
                        'active' => $subitem->active
                    ];
                }

                $subtext = false;
                if ($item->link == 'admin/notifications'){
                    $notifications = Notification::where('status','0')->get()->count();
                    if ($notifications > 0){
                        $subtext = ' +'.$notifications;
                    }
                }

                $menuitems[] = [
                    'title' => $item->title,
                    'link' => $item->link,
                    'active' => $item->active,
                    'icon' => $item->icon,
                    'children' => $children,
                    'subtext' => $subtext
                ];
            }

            if ($role_id == 1){
                $projects = 0;
            }
            else{
                $projects = 1;
            }
            $view->with(['menuitems' => $menuitems, 'projects' => $projects]);
        });

        View::composer('admin.dashboard.basic', function($view) {
            $role_id = Auth::user()->getRole();

            $projects_list = [];

            if ($role_id == 1){
                $projects = Project::get()->count();
                $projects_list = Project::get();
            }
            else if ($role_id == 2){
                $projects = Project::where('manager_id', Auth::user()->id)->get()->count();
                $projects_list = Project::where('manager_id', Auth::user()->id)->get();
            }
            else if ($role_id == 4){
                $projects = Project::where('client_id', Auth::user()->id)->get()->count();
                $projects_list = Project::where('client_id', Auth::user()->id)->get();
            }
            else if ($role_id == 3){
                $projects = Project::whereIn('id', function($query){
                    $query->select('project_id')
                    ->from(with(new ProjectUsers)->getTable())
                    ->where('user_id', Auth::user()->id);
                })->get()->count();

                $projects_list = Project::whereIn('id', function($query){
                    $query->select('project_id')
                    ->from(with(new ProjectUsers)->getTable())
                    ->where('user_id', Auth::user()->id);
                })->get();
            }

            $tasks = 0;
            if ($role_id == 1){
                $tasks = Task::get()->count();
            }
            else if ($role_id == 2){
                $tasks = Task::whereIn('project_id', function($query){
                    $query->select('project_id')
                    ->from(with(new Project)->getTable())
                    ->where('manager_id', Auth::user()->id);
                })->get()->count();
            }
            else if ($role_id == 4){
                $tasks = Task::whereIn('project_id', function($query){
                    $query->select('project_id')
                    ->from(with(new Project)->getTable())
                    ->where('client_id', Auth::user()->id);
                })->get()->count();
            }
            else if ($role_id == 3){
                $tasks = Task::whereIn('id', function($query){
                    $query->select('task_id')
                    ->from(with(new TaskUsers)->getTable())
                    ->where('user_id', Auth::user()->id);
                })->get()->count();
            }
            $view->with(['projects' => $projects, 'tasks' => $tasks, 'projects_list' => $projects_list]);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
