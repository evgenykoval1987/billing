<?php
namespace Laraspace;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laraspace\Models\Role;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'facebook_id', 'google_id', 'github_id', 'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin()
    {
        return true;
    }

    public function isLock()
    {
        return $this->lock;
    }

    public function getRole()
    {
        return $this->role_id;
    }

    public function getID()
    {
        return $this->id;
    }

    public static function login($request)
    {
        $remember = $request->remember;
        $email = $request->email;
        $password = $request->password;
        return (\Auth::attempt(['email' => $email, 'password' => $password], $remember));
    }

    public function role(){
        return $this->hasOne(Role::class, 'id', 'role_id');
    }

    /*public function project(){
        return $this->hasMany(Role::class, 'id', '_id');
    }*/
}
