-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.5.62 - MySQL Community Server (GPL)
-- Операционная система:         Win64
-- HeidiSQL Версия:              10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;



-- Дамп структуры для таблица bil.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы bil.failed_jobs: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Дамп структуры для таблица bil.jobs
CREATE TABLE IF NOT EXISTS `jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) unsigned NOT NULL,
  `reserved_at` int(10) unsigned DEFAULT NULL,
  `available_at` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_reserved_at_index` (`queue`(191),`reserved_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы bil.jobs: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;

-- Дамп структуры для таблица bil.menus
CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `active` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `parent_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_menus_roles` (`role_id`),
  CONSTRAINT `FK_menus_roles` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы bil.menus: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` (`id`, `role_id`, `title`, `link`, `active`, `icon`, `parent_id`) VALUES
	(1, 1, 'Пользователи', 'admin/users', 'admin/users*', 'icon-fa icon-fa-users', 0),
	(2, 1, 'Проекты', '#', 'admin/projects*', 'icon-fa icon-fa-tasks', 0),
	(4, 1, 'Добавить проект', 'admin/projects/add', 'admin/projects*', '', 2),
	(5, 1, 'Списко проектов', 'admin/projects', 'admin/projects*', '', 2);
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;

-- Дамп структуры для таблица bil.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы bil.migrations: ~7 rows (приблизительно)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2016_05_13_060834_create_settings_table', 1),
	(4, '2016_05_18_045906_create_todos_table', 1),
	(5, '2016_10_10_114222_add_providers_to_users_table', 1),
	(6, '2017_07_25_073654_create_jobs_table', 1),
	(7, '2017_07_25_073709_create_failed_jobs_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Дамп структуры для таблица bil.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`(191)),
  KEY `password_resets_token_index` (`token`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы bil.password_resets: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Дамп структуры для таблица bil.projects
CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `client_id` int(11) unsigned NOT NULL,
  `manager_id` int(11) unsigned NOT NULL,
  `status_id` int(11) unsigned NOT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `comment` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_projects_users` (`client_id`),
  KEY `FK_projects_users_2` (`manager_id`),
  KEY `FK_projects_statuses` (`status_id`),
  CONSTRAINT `FK_projects_statuses` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`id`),
  CONSTRAINT `FK_projects_users` FOREIGN KEY (`client_id`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_projects_users_2` FOREIGN KEY (`manager_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы bil.projects: ~7 rows (приблизительно)
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
INSERT INTO `projects` (`id`, `title`, `client_id`, `manager_id`, `status_id`, `date_start`, `date_end`, `comment`, `created_at`, `updated_at`) VALUES
	(1, 'test', 7, 8, 1, '2017-08-20', '2031-08-20', NULL, '2019-08-12 22:28:52', '2019-08-12 22:28:52'),
	(2, 'test2', 7, 9, 1, '2013-08-20', '2031-08-20', NULL, '2019-08-12 22:30:01', '2019-08-12 22:30:01'),
	(3, 'test2', 7, 9, 1, '2019-08-13', '2019-08-31', NULL, '2019-08-12 22:31:05', '2019-08-12 22:31:05'),
	(4, 'test', 7, 9, 2, '2019-08-15', '2019-08-31', NULL, '2019-08-12 22:36:53', '2019-08-12 22:36:53'),
	(5, 'test', 7, 9, 2, '2019-08-15', '2019-08-31', NULL, '2019-08-12 22:37:19', '2019-08-12 22:37:19'),
	(6, 'test', 7, 8, 2, '2019-08-14', '2019-08-30', '123', '2019-08-12 22:38:14', '2019-08-12 22:38:14'),
	(7, 'Billing system', 7, 10, 2, '2019-08-15', '2019-08-27', '<p>Первый проект</p><p>Первая задача</p>', '2019-08-12 22:48:53', '2019-08-13 23:46:41');
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;

-- Дамп структуры для таблица bil.project_comments
CREATE TABLE IF NOT EXISTS `project_comments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `text` text,
  `user_id` int(11) unsigned NOT NULL,
  `project_id` int(11) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_project_comments_users` (`user_id`),
  KEY `FK_project_comments_projects` (`project_id`),
  CONSTRAINT `FK_project_comments_projects` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_project_comments_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы bil.project_comments: ~10 rows (приблизительно)
/*!40000 ALTER TABLE `project_comments` DISABLE KEYS */;
INSERT INTO `project_comments` (`id`, `text`, `user_id`, `project_id`, `created_at`, `updated_at`) VALUES
	(1, '<p>fdffff</p>', 1, 7, '2019-08-14 00:22:34', '2019-08-14 00:22:34'),
	(2, '<p>sdfgsddfgsf</p>', 1, 7, '2019-08-14 00:28:59', '2019-08-14 00:28:59'),
	(3, '<p>sdfgsfdgsegrrgrh</p>', 1, 7, '2019-08-14 00:29:03', '2019-08-14 00:29:03'),
	(4, '<p>1111</p>', 1, 7, '2019-08-14 00:35:48', '2019-08-14 00:35:48'),
	(5, '<p>fff</p>', 1, 7, '2019-08-14 00:36:26', '2019-08-14 00:36:26'),
	(6, '<p>ddd</p>', 1, 7, '2019-08-14 00:36:43', '2019-08-14 00:36:43'),
	(7, '<p>sfgsjkbdfg</p><p>dsfgkjdfsg</p><p>sdfgsdfg</p>', 1, 7, '2019-08-14 00:37:12', '2019-08-14 00:37:12'),
	(8, '<p>tgetheth</p>', 1, 7, '2019-08-14 00:39:22', '2019-08-14 00:39:22'),
	(9, '<p>fhjhjrhj</p>', 1, 7, '2019-08-14 00:39:36', '2019-08-14 00:39:36'),
	(10, '<p>eryerth</p>', 1, 7, '2019-08-14 00:40:08', '2019-08-14 00:40:08');
/*!40000 ALTER TABLE `project_comments` ENABLE KEYS */;

-- Дамп структуры для таблица bil.project_users
CREATE TABLE IF NOT EXISTS `project_users` (
  `project_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`project_id`,`user_id`),
  KEY `FK_project_users_users` (`user_id`),
  CONSTRAINT `FK_project_users_projects` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_project_users_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы bil.project_users: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `project_users` DISABLE KEYS */;
INSERT INTO `project_users` (`project_id`, `user_id`) VALUES
	(6, 11),
	(7, 11),
	(6, 13),
	(7, 13);
/*!40000 ALTER TABLE `project_users` ENABLE KEYS */;

-- Дамп структуры для таблица bil.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы bil.roles: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `title`, `created_at`, `updated_at`) VALUES
	(1, 'Администратор', NULL, NULL),
	(2, 'Менеджер', NULL, NULL),
	(3, 'Исполнитель', NULL, NULL),
	(4, 'Клиент', NULL, NULL);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Дамп структуры для таблица bil.settings
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `option` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы bil.settings: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;

-- Дамп структуры для таблица bil.statuses
CREATE TABLE IF NOT EXISTS `statuses` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status_group_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_statuses_status_groups` (`status_group_id`),
  CONSTRAINT `FK_statuses_status_groups` FOREIGN KEY (`status_group_id`) REFERENCES `status_groups` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы bil.statuses: ~13 rows (приблизительно)
/*!40000 ALTER TABLE `statuses` DISABLE KEYS */;
INSERT INTO `statuses` (`id`, `title`, `description`, `status_group_id`) VALUES
	(1, 'Согласование', 'Согласование проекта с клиентом', 1),
	(2, 'В работе ', 'Есть рабочие задачи по проекту', 1),
	(3, 'В ожидании', 'Работы по проекту еще не начаты, но проект согласован с клиентом', 1),
	(4, 'Приостановлен', 'Отложен - сохранение этого статуса с указанием причины отказа в виде комментария в свободной форме', 1),
	(5, 'Отказ', 'Сохранение этого статуса с указанием причины отказа в виде комментария в свободной форме', 1),
	(7, 'Сдача проекта ', 'Проект завершен, но в проекте есть задачи в стадии «на проверке у клиента»', 1),
	(8, 'Согласование', NULL, 2),
	(9, 'В работе', NULL, 2),
	(10, 'На проверке у менеджера', NULL, 2),
	(11, 'На проверке у клиента', NULL, 2),
	(14, 'Доработка', NULL, 2),
	(15, 'Принята', NULL, 2),
	(16, 'Завершена', NULL, 2);
/*!40000 ALTER TABLE `statuses` ENABLE KEYS */;

-- Дамп структуры для таблица bil.status_groups
CREATE TABLE IF NOT EXISTS `status_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы bil.status_groups: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `status_groups` DISABLE KEYS */;
INSERT INTO `status_groups` (`id`, `title`) VALUES
	(1, 'Статусы проектов'),
	(2, 'Статусы задач');
/*!40000 ALTER TABLE `status_groups` ENABLE KEYS */;

-- Дамп структуры для таблица bil.tasks
CREATE TABLE IF NOT EXISTS `tasks` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` text,
  `description` text,
  `project_id` int(11) unsigned NOT NULL,
  `status_id` int(11) unsigned NOT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_tasks_projects` (`project_id`),
  KEY `FK_tasks_statuses` (`status_id`),
  CONSTRAINT `FK_tasks_projects` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_tasks_statuses` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы bil.tasks: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;
INSERT INTO `tasks` (`id`, `title`, `description`, `project_id`, `status_id`, `date_start`, `date_end`, `created_at`, `updated_at`) VALUES
	(1, 'test', '<p>test 111</p>', 7, 14, '2019-08-15', '2019-08-24', '2019-08-14 02:34:38', '2019-08-13 23:34:38'),
	(2, '222', '<p>22222</p>', 7, 9, '2019-08-21', '2019-08-26', '2019-08-13 22:52:28', '2019-08-13 22:52:28'),
	(5, 'test 4', '<p>adfdsfadsf</p><p>adsf</p><p>asdf</p>', 7, 8, '2019-08-22', '2019-08-23', '2019-08-13 23:38:52', '2019-08-13 23:38:52');
/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;

-- Дамп структуры для таблица bil.task_users
CREATE TABLE IF NOT EXISTS `task_users` (
  `task_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`task_id`,`user_id`),
  KEY `FK_task_users_users` (`user_id`),
  CONSTRAINT `FK_task_users_tasks` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_task_users_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы bil.task_users: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `task_users` DISABLE KEYS */;
INSERT INTO `task_users` (`task_id`, `user_id`) VALUES
	(1, 11),
	(2, 11),
	(5, 13);
/*!40000 ALTER TABLE `task_users` ENABLE KEYS */;

-- Дамп структуры для таблица bil.todos
CREATE TABLE IF NOT EXISTS `todos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы bil.todos: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `todos` DISABLE KEYS */;
/*!40000 ALTER TABLE `todos` ENABLE KEYS */;

-- Дамп структуры для таблица bil.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int(11) unsigned NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `facebook_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `github_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lock` int(1) DEFAULT '0',
  `company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_users_roles` (`role_id`),
  CONSTRAINT `FK_users_roles` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы bil.users: ~9 rows (приблизительно)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `password`, `role_id`, `remember_token`, `created_at`, `updated_at`, `facebook_id`, `google_id`, `github_id`, `lock`, `company`) VALUES
	(1, 'Коваль Евгений Владимирович', 'evgenykoval@gmail.com', '$2y$10$ccWX8cVo2OLP/jFaRnnpTu7o1rNy1DGlgHawFyVhJuxlmcS.OIIua', 1, 'zf2HMzN6woroYeBUMnOHH1V12Jj3ta40FgW42HYvxzQme7bWDt1hP4BJOcSC', '2019-08-04 22:19:47', '2019-08-07 22:07:50', NULL, NULL, NULL, 0, NULL),
	(7, 'Иванов Иван Иванович1', 'ceskf@mail.ru', '$2y$10$ccWX8cVo2OLP/jFaRnnpTu7o1rNy1DGlgHawFyVhJuxlmcS.OIIua', 4, 'V2XlhdaVRMd9ySDoeX1461gPaE8zkR9k94n051bEGyXycuIZJyKyOL4pWlPy', '2019-08-04 22:19:47', '2019-08-08 21:19:15', NULL, NULL, NULL, 0, 'ТОВ Red Bull'),
	(8, 'Иванов Иван Иванович', 'ceskf@mail.ru', '$2y$10$ccWX8cVo2OLP/jFaRnnpTu7o1rNy1DGlgHawFyVhJuxlmcS.OIIua', 2, 'V2XlhdaVRMd9ySDoeX1461gPaE8zkR9k94n051bEGyXycuIZJyKyOL4pWlPy', '2019-08-04 22:19:47', '2019-08-07 21:12:51', NULL, NULL, NULL, 0, NULL),
	(9, 'Иванов Иван Иванович', 'ceskf@mail.ru', '$2y$10$ccWX8cVo2OLP/jFaRnnpTu7o1rNy1DGlgHawFyVhJuxlmcS.OIIua', 2, 'V2XlhdaVRMd9ySDoeX1461gPaE8zkR9k94n051bEGyXycuIZJyKyOL4pWlPy', '2019-08-04 22:19:47', '2019-08-07 21:12:51', NULL, NULL, NULL, 0, NULL),
	(10, 'Иванов Иван Иванович', 'ceskf@mail.ru', '$2y$10$ccWX8cVo2OLP/jFaRnnpTu7o1rNy1DGlgHawFyVhJuxlmcS.OIIua', 2, 'V2XlhdaVRMd9ySDoeX1461gPaE8zkR9k94n051bEGyXycuIZJyKyOL4pWlPy', '2019-08-04 22:19:47', '2019-08-07 21:12:51', NULL, NULL, NULL, 0, NULL),
	(11, 'Иванов Иван Иванович', 'ceskf@mail.ru', '$2y$10$ccWX8cVo2OLP/jFaRnnpTu7o1rNy1DGlgHawFyVhJuxlmcS.OIIua', 3, 'V2XlhdaVRMd9ySDoeX1461gPaE8zkR9k94n051bEGyXycuIZJyKyOL4pWlPy', '2019-08-04 22:19:47', '2019-08-07 21:12:51', NULL, NULL, NULL, 0, NULL),
	(12, 'Иванов Иван Иванович', 'ceskf@mail.ru', '$2y$10$ccWX8cVo2OLP/jFaRnnpTu7o1rNy1DGlgHawFyVhJuxlmcS.OIIua', 2, 'V2XlhdaVRMd9ySDoeX1461gPaE8zkR9k94n051bEGyXycuIZJyKyOL4pWlPy', '2019-08-04 22:19:47', '2019-08-07 21:12:51', NULL, NULL, NULL, 0, NULL),
	(13, 'Петров Иван Иванович', 'ceskf@mail.ru', '$2y$10$ccWX8cVo2OLP/jFaRnnpTu7o1rNy1DGlgHawFyVhJuxlmcS.OIIua', 3, 'V2XlhdaVRMd9ySDoeX1461gPaE8zkR9k94n051bEGyXycuIZJyKyOL4pWlPy', '2019-08-04 22:19:47', '2019-08-07 21:12:51', NULL, NULL, NULL, 0, NULL),
	(14, 'Иванов Иван Иванович', 'ceskf@mail.ru', '$2y$10$ccWX8cVo2OLP/jFaRnnpTu7o1rNy1DGlgHawFyVhJuxlmcS.OIIua', 2, 'V2XlhdaVRMd9ySDoeX1461gPaE8zkR9k94n051bEGyXycuIZJyKyOL4pWlPy', '2019-08-03 22:19:47', '2019-08-07 22:08:00', NULL, NULL, NULL, 0, NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
