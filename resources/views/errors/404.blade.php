<!DOCTYPE html>
<html>
<head>
    <title>Billing system - Ошибка</title>
    <script src="{{asset('/assets/admin/js/core/pace.js')}}"></script>
    <link href="{{ mix('/assets/admin/css/laraspace.css') }}" rel="stylesheet" type="text/css">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    @include('admin.layouts.partials.favicons')
</head>
<body id="app" class="page-error-404 skin-tyrell">
    <header class="site-header">
        <a href="#" class="brand-main">
            <h3>Billing system</h3>
        </a>
        <a href="#" class="nav-toggle">
            <div class="hamburger hamburger--htla">
                <span>toggle menu</span>
            </div>
        </a>
    </header>
    <div class="error-box">
        <div class="row">
            <div class="col-sm-12 text-sm-center">
                <h1 style="color: #4fc47f">404</h1>
                <h5>Страница недоступна!</h5>
                <a class="btn btn-lg bg-tyrell" href="/admin"> <i class="icon-fa icon-fa-arrow-left"></i> Вернуться на главную</a>
            </div>
        </div>
    </div>
    <script src="{{mix('/assets/admin/js/core/plugins.js')}}"></script>
    @yield('scripts')
</body>
</html>
