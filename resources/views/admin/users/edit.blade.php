<form id="user-edit-form" novalidate action="{{route('admin.users.edit', $user->id)}}">
    <div class="form-group">
        <label>Email<small>(логин)</small><span class="reqfield">*</span></label> 
        <input type="email" name="email" placeholder="Email" class="form-control" value="{{$user->email}}">
    </div> 
    <div class="form-group">
        <label>Роль<span class="reqfield">*</span></label> 
        <select name="role_id" class="form-control" data-edit-role-id>
            <option></option>
            @foreach($roles as $role)
                @if ($role->id == $user->role_id)
                    <option value="{{$role->id}}" selected>{{$role->title}}</option>
                @else
                    <option value="{{$role->id}}">{{$role->title}}</option>
                @endif
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label>ФИО</label> 
        <input type="text" name="name" placeholder="ФИО" class="form-control" value="{{$user->name}}">
    </div> 
    <div class="form-group" style="display: none;" data-edit-company>
        <label>Компания</label> 
        <input type="text" name="company" placeholder="Компания" class="form-control" value="{{$user->company}}">
    </div> 
    <div class="form-group"><small class="text-muted">Если не хотите менять пароль - оставьте поля паролей пустыми.</small></div>
    <div class="form-group">
        <label>Пароль</label> 
        <input type="password" name="password" id="epassword" placeholder="Пароль" class="form-control">
    </div> 
    <div class="form-group">
        <label>Повторите пароль</label> 
        <input type="password" name="password_confirmation" placeholder="Повторите пароль" class="form-control">
    </div>
    
</form>