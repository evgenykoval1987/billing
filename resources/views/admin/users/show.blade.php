@extends('admin.layouts.layout-horizontal')

@section('scripts')
    <script src="/assets/admin/js/users/users.js"></script>
@stop

@section('content')
    <div class="main-content page-profile">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="tabs tabs-default">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#profile" role="tab">Профиль</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#messages" role="tab">Проекты</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="profile" role="tabpanel">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h4>{{$user->name}}</h4>
                                            <p class="detail-row"><i class="icon-fa icon-fa-envelope"></i> {{$user->email}}</p>
                                            <p class="detail-row" title="Группа пользователей"><i class="icon-fa icon-fa-users"></i> {{$user->role->title}}</p>
                                            @if($user->role_id == 4 && $user->company != '')
                                                <p class="detail-row" title="Компания"><i class="icon-fa icon-fa-building"></i> {{$user->company}}</p>
                                            @endif
                                            <p class="detail-row" title="Дата регистрации"><i class="icon-fa icon-fa-calendar"></i> {{$user->created_at}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="messages" role="tabpanel">
                                    <ul class="media-list activity-list">
                                        <li class="media">
                                            <div class="media-left">
                                                <a href="#">
                                                    <img class="media-object img-thumbnail" src="/assets/admin/img/avatars/avatar1.png" alt="Generic placeholder image">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">Adam David <span>sent a message</span></h4>
                                                <small>Today at 3.50pm</small>
                                                <p class="mt-2">"When you have children, you always have family. They will always be your priority, your responsibility.
                                                    And a man, a man provides. And he does it even when he's not appreciated or respected or even loved. He simply bears up and he does it. Because he's a man."</p>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <div class="media-left">
                                                <a href="#">
                                                    <img class="media-object img-thumbnail" src="/assets/admin/img/avatars/avatar2.png" alt="Generic placeholder image">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">Shane White <span>sent a message</span></h4>
                                                <small>Yesterday at 9pm</small>
                                                <p class="mt-2">
                                                    “Hey! How you doin?”
                                                </p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
