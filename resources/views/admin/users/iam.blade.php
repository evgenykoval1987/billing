@extends('admin.layouts.layout-horizontal')

@section('scripts')
    <script src="/assets/admin/js/users/iam.js"></script>
@stop

@section('content')
    <div class="main-content">
        <div class="page-header">
            <h3 class="page-title">Мой профиль</h3>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header">
                        <h6>Редактировать</h6>
                    </div>
                    <div class="card-body">
                        <form data-iam-form method="POST" action="{{route('admin.iam')}}" id="user-iam-form">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label for="name">ФИО</label>
                                <input type="text" class="form-control" id="name" placeholder="ФИО" name="name" value="{{$user->name}}">
                            </div>
                            <div class="form-group" style="display: none;" data-edit-company>
                                <label>Компания</label> 
                                <input type="text" name="company" placeholder="Компания" class="form-control" value="{{$user->company}}">
                            </div> 
                            <div class="form-group"><small class="text-muted">Если не хотите менять пароль - оставьте поля паролей пустыми.</small></div>
                            <div class="form-group">
                                <label>Пароль</label> 
                                <input type="password" name="password" id="epassword" placeholder="Пароль" class="form-control">
                            </div> 
                            <div class="form-group">
                                <label>Повторите пароль</label> 
                                <input type="password" name="password_confirmation" placeholder="Повторите пароль" class="form-control">
                            </div>
                            <input type="hidden" name="id" value="{{$user->id}}">
                            <button class="btn btn-success" data-user-edit-button>Сохранить</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header">
                        <h6>Информация</h6>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" class="form-control" id="email" name="email" value="{{$user->email}}" disabled="disabled">
                        </div>
                        <div class="form-group">
                            <label for="email">Роль</label>
                            <input type="text" class="form-control" id="role" name="role" value="{{$user->role->title}}" disabled="disabled">
                            <input type="hidden" data-edit-role-id value="{{$user->role_id}}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@stop
