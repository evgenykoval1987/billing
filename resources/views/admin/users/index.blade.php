@extends('admin.layouts.layout-horizontal')

@section('scripts')
    <script src="/assets/admin/js/users/users.js"></script>
@stop

@section('content')
    <div class="main-content">
        <div class="page-header">
            <h3 class="page-title">Пользователи</h3>
            <div class="page-actions">
                <a href="#" class="btn btn-success btn-xs" data-toggle="modal" data-target="#user-add"><i class="icon-fa icon-fa-plus"></i> Добавить</a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    
                    <div class="card-body">
                        <table id="users-datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>ФИО</th>
                                <th>Email</th>
                                <th>Роль</th>
                                <th>Дата регистрации</th>
                                <th>Действия</th>
                            </tr>
                            </thead>
                            
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="user-add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" class="modal fade" aria-hidden="true" style="display: none;">
        <div role="document" class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-success">
                    <h5 id="exampleModalLabel" class="modal-title">Добавить пользователя</h5> 
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div> 
                <div class="modal-body">
                    <form id="user-add-form" novalidate action="{{route('admin.users.add')}}">
                        <div class="form-group">
                            <label>Email<small>(логин)</small><span class="reqfield">*</span></label> 
                            <input type="email" name="email" placeholder="Email" class="form-control">
                        </div> 
                        <div class="form-group">
                            <label>Роль<span class="reqfield">*</span></label> 
                            <select name="role_id" class="form-control" data-role-id>
                                <option></option>
                                @foreach($roles as $role)
                                    <option value="{{$role->id}}">{{$role->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Пароль<span class="reqfield">*</span></label> 
                            <input type="password" name="password" id="password" placeholder="Пароль" class="form-control">
                        </div> 
                        <div class="form-group">
                            <label>Повторите пароль<span class="reqfield">*</span></label> 
                            <input type="password" name="password_confirmation" placeholder="Повторите пароль" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>ФИО</label> 
                            <input type="text" name="name" placeholder="ФИО" class="form-control">
                        </div> 
                        <div class="form-group" style="display: none;" data-company>
                            <label>Компания</label> 
                            <input type="text" name="company" placeholder="Компания" class="form-control">
                        </div> 
                    </form>
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-user-add-button>Добавить</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                </div>
            </div>
        </div>
    </div>

    <div id="user-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" class="modal fade" aria-hidden="true" style="display: none;">
        <div role="document" class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-success">
                    <h5 id="exampleModalLabel" class="modal-title">Редактировать пользователя</h5> 
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div> 
                <div class="modal-body">
                    
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-user-edit-button>Сохранить</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                </div>
            </div>
        </div>
    </div>
@stop
