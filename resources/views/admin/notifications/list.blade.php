@extends('admin.layouts.layout-horizontal')

@section('scripts')
    <script src="/assets/admin/js/notifications/list.js"></script>
@stop

@section('content')
    <div class="main-content">
        <div class="page-header">
            <h4 class="page-title">Мои уведомления</h4>
            <div class="page-actions">
                <a href="{{route('admin.notifications.clear')}}" class="btn btn-danger btn-xs"><i class="icon-fa icon-fa-trash"></i> Очистить</a>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-xl-6 mb-4 col-sm-6 col-md-6 col-sm-6">
                        <h5 class="section-semi-title">Новые уведомления</h5>
                        <table class="table">
                            <tbody>
                                @foreach($notifications as $notification)
                                    @if($notification->status == 0)
                                        <tr>
                                            <td>
                                                {!!$notification->text!!}
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="col-xl-6 mb-4 col-sm-6 col-md-6 col-sm-6">
                        <h5 class="section-semi-title">Прочитанные уведомления</h5>
                        <table class="table">
                            <tbody>
                                 @foreach($notifications as $notification)
                                    @if($notification->status == 1)
                                        <tr>
                                            <td>
                                                {!!$notification->text!!}
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop