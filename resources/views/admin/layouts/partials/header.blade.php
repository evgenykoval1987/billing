<header class="site-header">
  <a href="{{route('admin.dashboard')}}" class="brand-main">
    <h3>Billing system</h3>
  </a>
  <a href="#" class="nav-toggle">
    <div class="hamburger hamburger--htla">
      <span>toggle menu</span>
    </div>
  </a>
  <ul class="action-list">
    {{--<li>
      <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="icon-fa icon-fa-bell"></i></a>
      <div class="dropdown-menu dropdown-menu-right notification-dropdown">
        <h6 class="dropdown-header">Notifications</h6>
        <a class="dropdown-item" href="#"><i class="icon-fa icon-fa-user"></i> New User was Registered</a>
        <a class="dropdown-item" href="#"><i class="icon-fa icon-fa-comment"></i> A Comment has been posted.</a>
      </div>
    </li>--}}
    <li>
      <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="avatar"><img src="{{asset('/assets/admin/img/avatars/avatar.png')}}" alt="Avatar"></a>
      <div class="dropdown-menu dropdown-menu-right notification-dropdown">
        <a class="dropdown-item" href="{{route('admin.iam')}}"><i class="icon-fa icon-fa-cogs"></i> Профиль</a>
        <a class="dropdown-item" href="/logout"><i class="icon-fa icon-fa-sign-out"></i> Выйти</a>
      </div>
    </li>
  </ul>
</header>
