<!DOCTYPE html>
<html>
    <head>
        <title>Billing System - Dashboard</title>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700' rel='stylesheet' type='text/css'>
        <script src="{{asset('/assets/admin/js/core/pace.js')}}"></script>
        <link href="{{ mix('/assets/admin/css/laraspace.css') }}" rel="stylesheet" type="text/css">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        @include('admin.layouts.partials.favicons')
        @yield('styles')
    </head>
    <body class="layout-horizontal skin-tyrell">

        <div id="app" class="site-wrapper">
            @include('admin.layouts.partials.laraspace-notifs')
            @include('admin.layouts.partials.header')
            <div class="mobile-menu-overlay"></div>
            @include('admin.layouts.partials.header-bottom')
            @yield('content')
            @include('admin.layouts.partials.footer')
        </div>
        <script src="{{asset('/assets/admin/js/calendar/ru.js')}}"></script>
        <script src="{{mix('/assets/admin/js/core/plugins.js')}}"></script>
        <script src="{{asset('/assets/admin/js/demo/skintools.js')}}"></script>
        <script src="{{mix('/assets/admin/js/core/app.js')}}"></script>
        
        <script type="text/javascript">
            var datatavlelang = {
                "processing": "Подождите...",
                "search": "Поиск:",
                "lengthMenu": "Показать _MENU_ записей",
                "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
                "infoEmpty": "Записи с 0 до 0 из 0 записей",
                "infoFiltered": "(отфильтровано из _MAX_ записей)",
                "infoPostFix": "",
                "loadingRecords": "Загрузка записей...",
                "zeroRecords": "Записи отсутствуют.",
                "emptyTable": "В таблице отсутствуют данные",
                "paginate": {
                    "first": "<",
                    "previous": "<<",
                    "next": ">>",
                    "last": ">"
                },
                "aria": {
                    "sortAscending": ": активировать для сортировки столбца по возрастанию",
                    "sortDescending": ": активировать для сортировки столбца по убыванию"
                }
            }
        </script>
        @yield('scripts')
    </body>
</html>
