@extends('admin.layouts.layout-horizontal')

@section('scripts')
    <script src="/assets/admin/js/reports/tasks.js"></script>
@stop

@section('content')
    <div class="main-content">
        <div class="page-header">
            <h3 class="page-title">Задачи в работе</h3>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <table id="tasks-datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Название</th>
                                <th>Проект</th>
                                <th>Клиент</th>
                                <th>Менеджер</th>
                                <th>Исполнители</th>
                                <th>Сроки</th>
                                <th>Статус</th>
                            </tr>
                            </thead>
                            
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
