@extends('admin.layouts.layout-horizontal')

@section('scripts')
    <script src="/assets/admin/js/projects/add.js"></script>
@stop

@section('content')
	<div class="main-content">
        <div class="page-header">
            <h3 class="page-title">{{$h1}}</h3>
        </div>
    
	    <form data-project-form method="POST" action="{{$action}}">
	    	{!! csrf_field() !!}
	    	<div class="row">
	            <div class="col-sm-6">
	            	<div class="card">
	            		<div class="card-header">
	                        <h6>Информация</h6>
	                    </div>
                    	<div class="card-body">
			            	<div class="form-group">
			                    <label for="name">Название</label>
			                    <input type="text" class="form-control" id="name" name="name" placeholder="Название" value="{{isset($info) ? $info->title : ''}}">
			                </div>

			                <div class="form-group">
			                    <label for="manager_id">Сроки проекта</label>
			                    <div class="input-group input-daterange">
		                            <input type="text" class="form-control ls-datepicker" name="date_start" value="{{isset($info) ? $info->date_start : ''}}">
		                            <div class="input-group-prepend input-group-append">
		                                <span class="input-group-text">по</span>
		                            </div>
		                            <input type="text" class="form-control ls-datepicker" name="date_end"  value="{{isset($info) ? $info->date_end : ''}}">
		                        </div>
			                </div>

			                <div class="form-group">
			                	<label for="status_id">Статус</label> 
			                	<select id="status_id" class="form-control" name="status_id">
			                		<option value="">- Выберите статус -</option>
			                		@foreach($statuses as $status)
			                			@if($status->status_group_id == 1)
		                            		<option value="{{$status->id}}" {{isset($info) && $info->status_id == $status->id ? 'selected' : ''}}>{{$status->title}}</option>
		                            	@endif
		                            @endforeach
			                	</select>
			                </div>

			                <div class="form-group">
			                	<label for="comment">Описание</label> 
			                	<textarea id="comment" rows="3" class="form-control ls-summernote" name="comment">{{isset($info) ? $info->comment : ''}}</textarea>
			                </div>
			            </div>
		            </div>
	            </div>
	            <div class="col-sm-6">
	            	<div class="card">
	            		<div class="card-header">
	                        <h6>Участники</h6>
	                    </div>
                    	<div class="card-body">
                    		<div class="form-group">
			                    <label for="client_id">Клиент</label>
			                    <select class="form-control" id="client_id" name="client_id" {{($client_id) ? 'disabled' : ''}}>
			                    	<option value="">- Выберите клиента -</option>
		                            @foreach($users as $user)
		                            	@if($client_id)
			                            	@if($user->role_id == 4 && $client_id == $user->id)
			                            		<option value="{{$user->id}}" {{isset($info) && $info->client_id == $user->id ? 'selected' : ''}} selected="selected">{{$user->name}} ({{$user->company}})</option>
			                            	@endif
			                            @else
			                            	@if($user->role_id == 4)
			                            		<option value="{{$user->id}}" {{isset($info) && $info->client_id == $user->id ? 'selected' : ''}}>{{$user->name}} ({{$user->company}})</option>
			                            	@endif
			                            @endif
		                            @endforeach
		                        </select>
			                </div>

			                <div class="form-group">
			                    <label for="manager_id">Менеджер проекта</label>
			                    <select class="form-control" id="manager_id" name="manager_id">
			                    	<option value="">- Выберите менеджера -</option>
		                            @foreach($users as $user)
		                            	@if($user->role_id == 2)
		                            		<option value="{{$user->id}}" {{isset($info) && $info->manager_id == $user->id ? 'selected' : ''}}>{{$user->name}}</option>
		                            	@endif
		                            @endforeach
		                        </select>
			                </div>

			                <div class="form-group">
			                	<label for="workers">Исполнители</label>
	                			<select class="form-control ls-multi-select" multiple="multiple" name="workers[]" id="workers">
		                            @foreach($users as $user)
		                            	@if($user->role_id == 3)
		                            		<option value="{{$user->id}}" {{isset($workers) && in_array($user->id, $workers) ? 'selected' : ''}}>{{$user->name}}</option>
		                            	@endif
		                            @endforeach
		                        </select>
			                </div>
                    	</div>
                    </div>
	            </div>
	        </div>
	        <div class="row">
	        	<div class="col-sm-12 text-center">
	        		@if(isset($info))
	        			<button class="btn btn-success">Сохранить</button>
	        		@else
	        			<button class="btn btn-success">Добавить</button>
	        		@endif
	        	</div>
	        </div>
	    </form>
    </div>
@stop
