@extends('admin.layouts.layout-horizontal')

@section('scripts')
    <script src="/assets/admin/js/projects/list.js"></script>
@stop

@section('content')
    <div class="main-content">
        <div class="page-header">
            <h3 class="page-title">Проекты</h3>
            @if($add)
                <div class="page-actions">
                    <a href="{{route('admin.projects.add')}}" class="btn btn-success btn-xs"><i class="icon-fa icon-fa-plus"></i> Добавить</a>
                </div>
            @endif
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <table id="projects-datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Название</th>
                                <th>Клиент</th>
                                <th>Сроки</th>
                                <th>Статус</th>
                                <th>Действия</th>
                            </tr>
                            </thead>
                            
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
