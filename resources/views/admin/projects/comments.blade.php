<ul class="media-list activity-list">
	@foreach($comments as $comment)
		<li class="media">
			<div class="media-body">
				<h5 class="media-heading">{{$comment->user->name}}</h5>
				<small>{{date('d.m.Y H:i', strtotime($comment->created_at))}}</small> 
				<p class="mt-2">{!! $comment->text !!}</p>
			</div>
		</li>
	@endforeach
</ul>