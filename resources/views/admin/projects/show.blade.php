@extends('admin.layouts.layout-horizontal')

@section('scripts')
    <script src="/assets/admin/js/tasks/list.js"></script>
    <script src="/assets/admin/js/projects/show.js"></script>
@stop

@section('content')
    <div class="main-content page-profile">
        <div class="page-header">
            <h4 class="page-title">Проект {{$info->title}}</h4>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="tabs tabs-default">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#overview" role="tab">Обзор</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#tasks" role="tab">Задачи</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="overview" role="tabpanel">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="card">
                                                <div class="card-header"><h6>Информация</h6></div>
                                                <div class="card-body">
                                                    <h5 class="section-semi-title">Проект</h5>
                                                    <p>{{$info->title}}</p>
                                                    <h5 class="section-semi-title">Сроки</h5>
                                                    <p>{{date('d.m.Y',strtotime($info->date_start))}} - {{date('d.m.Y',strtotime($info->date_end))}}</p>
                                                    <input type="hidden" id="project_start_date" value="{{date('d.m.Y',strtotime($info->date_start))}}">
                                                    <input type="hidden" id="project_end_date" value="{{date('d.m.Y',strtotime($info->date_end))}}">
                                                    <h5 class="section-semi-title">Статус</h5>
                                                    <p>{{$info->status->title}}</p>
                                                    <h5 class="section-semi-title">Описание</h5>
                                                    <p>{!! $info->comment !!}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="card">
                                                <div class="card-header"><h6>Комментарии</h6></div>
                                                <div class="card-body">
                                                    <div id="comments" data-href="{{route('admin.projects.comment.get', $info->id)}}">
                                                        
                                                    </div>
                                                    <form action="{{route('admin.projects.comment.add')}}" id="form-add-comment" novalidate>
                                                        <input type="hidden" name="project_id" value="{{$info->id}}">
                                                        <textarea class="ls-summernote" id="comment" name="comment"></textarea>
                                                        <div class="text-center" style="margin-top:10px;">
                                                            <button type="button" class="btn btn-success" data-add-comment>Добавить</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="card">
                                                <div class="card-header"><h6>Участники</h6></div>
                                                <div class="card-body">
                                                    <h5 class="section-semi-title">Добавил</h5>
                                                    <p>{{$info->user->name}}</p>
                                                    <h5 class="section-semi-title">Клиент</h5>
                                                    <p>{{$info->client->name}}</p>
                                                    <h5 class="section-semi-title">Менеджер</h5>
                                                    <p>{{$info->manager->name}}</p>
                                                    <h5 class="section-semi-title">Исполнители</h5>
                                                    @foreach($info->project_users as $worker)
                                                        <p>{{$worker->user->name}}</p>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tasks" role="tabpanel">
                                    @if($add)
                                        <div class="text-right">
                                            <a href="#" class="btn btn-success btn-xs" data-toggle="modal" data-target="#task-add"><i class="icon-fa icon-fa-plus"></i> Добавить</a>
                                        </div>
                                    @endif
                                    <div style="clear: both; margin-top: 30px">
                                        <table id="tasks-datatable" class="table table-sm" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th>Дата</th>
                                                <th>Название</th>
                                                <th>Сроки</th>
                                                <th>Статус</th>
                                                <th>Действия</th>
                                            </tr>
                                            </thead>
                                            
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="task-add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" class="modal fade " aria-hidden="true" style="display: none;">
        <div role="document" class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header bg-success">
                    <h5 id="exampleModalLabel" class="modal-title">Добавить задачу</h5> 
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div> 
                <div class="modal-body">
                    <form id="task-add-form" novalidate action="{{route('admin.tasks.add')}}">
                        <input type="hidden" name="project_id" id="project_id" value="{{$info->id}}">
                        <div class="form-group">
                            <label>Название задачи</label> 
                            <input type="input" name="name" id="name" placeholder="Название задачи" class="form-control">
                        </div> 
                        <div class="form-group">
                            <label>Описание</label> 
                            <textarea class="ls-summernote" name="description" id="description"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="manager_id">Сроки</label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control lsd-datepicker" name="date_start">
                                <div class="input-group-prepend input-group-append">
                                    <span class="input-group-text">по</span>
                                </div>
                                <input type="text" class="form-control lsd-datepicker" name="date_end">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="status_id">Статус</label> 
                            <select id="status_id" class="form-control" name="status_id">
                                <option value="">- Выберите статус -</option>
                                @foreach($statuses as $status)
                                    @if($status->status_group_id == 2)
                                        <option value="{{$status->id}}">{{$status->title}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="workers">Исполнители</label>
                            <select class="form-control ls-multi-select" multiple="multiple" name="workers[]" id="workers">
                                @foreach($workers as $worker)
                                    @if($worker->user->role_id == 3)
                                        <option value="{{$worker->user_id}}">{{$worker->user->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </form>
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-task-add-button>Добавить</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                </div>
            </div>
        </div>
    </div>

    <div id="task-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" class="modal fade" aria-hidden="true" style="display: none;">
        <div role="document" class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-success">
                    <h5 id="exampleModalLabel" class="modal-title">Редактировать задачу</h5> 
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div> 
                <div class="modal-body">
                    
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-task-edit-button>Сохранить</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                </div>
            </div>
        </div>
    </div>
@stop
