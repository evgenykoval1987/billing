@extends('admin.layouts.layout-horizontal')

@section('scripts')
    <script src="/assets/admin/js/calendar/index.js"></script>
@stop
@section('styles')
    <style type="text/css">
    	.fc-day-grid-event .fc-content {
		  	white-space: pre-wrap;
		  	overflow: hidden;
		}
    </style>
@stop

@section('content')
    <div class="main-content">
        <div class="page-header">
            <h4 class="page-title">Календарь</h4>
            <div class="page-actions">
                <a href="#" class="btn btn-success btn-xs" data-toggle="modal" data-target="#event-add"><i class="icon-fa icon-fa-plus"></i> Добавить событие</a>
            </div>
        </div>
        <div class="calendar">
            <div class="row">
                <div class="col-xl-2 calendar-event">
                    <div class="card">
                        <div class="card-header">
                            <h5>События</h5>
                        </div>
                        <div class="card-body">
                            <ol class="fc-event-list">
                            	@foreach ($dots as $dot)
                            		<li><span class="{{$dot['label']}}"></span> {{$dot['title']}}</li>
                            	@endforeach
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="col-xl-10">
                    <div class="card">
                        <div class="card-body">
                            <div id='calendar'></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="event-add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" class="modal fade" aria-hidden="true" style="display: none;">
        <div role="document" class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-success">
                    <h5 id="exampleModalLabel" class="modal-title">Добавить событие</h5> 
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div> 
                <div class="modal-body">
                    <form id="event-add-form" novalidate action="{{route('admin.calendar.add')}}">
                        <div class="form-group">
                            <label>Название<span class="reqfield">*</span></label> 
                            <input type="text" name="title" placeholder="Название" class="form-control">
                        </div> 
                        <div class="form-group">
                            <label>Описание</label> 
                            <textarea name="description" placeholder="Описание" class="form-control"></textarea>
                        </div> 
                        <div class="form-group">
                            <label>Дата<span class="reqfield">*</span></label> 
                            <input type="text" name="dt" placeholder="Дата" class="form-control ls-datepicker">
                        </div> 
                        <div class="form-group">
		                	<label for="status_id">Тип события<span class="reqfield">*</span></label> 
		                	<select id="status_id" class="form-control" name="status_id">
		                		<option value="">- Выберите тип -</option>
		                		@foreach($statuses as $status)
	                            	<option value="{{$status->id}}" name="status_id">{{$status->title}}</option>
	                            @endforeach
		                	</select>
		                </div>
                    </form>
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-event-add-button>Добавить</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                </div>
            </div>
        </div>
    </div>

    <div id="event-view" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" class="modal fade" aria-hidden="true" style="display: none;">
        <div role="document" class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-success">
                    <h5 id="exampleModalLabel" class="modal-title"></h5> 
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div> 
                <div class="modal-body">
                	<h4>Описание</h4>
                	<div class="description" style="margin-bottom: 20px"></div>
                	<h4>Дата</h4>
                	<div class="dt" style="margin-bottom: 20px"></div>
                	<h4>Тип события</h4>
                	<div class="type"></div>
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Выйти</button>
                </div>
            </div>
        </div>
    </div>
@stop