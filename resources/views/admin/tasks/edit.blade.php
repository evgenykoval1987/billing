<form id="task-edit-form" novalidate action="{{route('admin.tasks.edit-task', $info->id)}}">
    <div class="form-group">
        <label>Название задачи</label> 
        <input type="input" name="name" id="name" placeholder="Название задачи" class="form-control" value="{{$info->title}}">
    </div> 
    <div class="form-group">
        <label>Описание</label> 
        <textarea class="ls-summernote" name="description" id="description">{{$info->description}}</textarea>
    </div>
    <div class="form-group">
        <label for="manager_id">Сроки</label>
        <div class="input-group input-daterange">
            <input type="text" class="form-control lsd-datepicker" name="date_start" value="{{date('d.m.Y',strtotime($info->date_start))}}">
            <div class="input-group-prepend input-group-append">
                <span class="input-group-text">по</span>
            </div>
            <input type="text" class="form-control lsd-datepicker" name="date_end" value="{{date('d.m.Y',strtotime($info->date_end))}}">
        </div>
    </div>
    <div class="form-group">
        <label for="status_id">Статус</label> 
        <select id="status_id" class="form-control" name="status_id">
            <option value="">- Выберите статус -</option>
            @foreach($statuses as $status)
                @if($status->status_group_id == 2)
                    <option value="{{$status->id}}" {{($info->status_id == $status->id) ? 'selected' : ''}}>{{$status->title}}</option>
                @endif
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="workers">Исполнители</label>
        <select class="form-control ls-multi-select" multiple="multiple" name="workers[]" id="workers">
            @foreach($workers as $worker)
                @if($worker->user->role_id == 3)
                    <option value="{{$worker->user_id}}" {{(in_array($worker->user_id, $workers_in)) ? 'selected' : ''}}>{{$worker->user->name}}</option>
                @endif
            @endforeach
        </select>
    </div>
</form>