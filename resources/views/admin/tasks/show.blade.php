@extends('admin.layouts.layout-horizontal')

@section('scripts')
    {{--<script src="/assets/admin/js/tasks/list.js"></script>--}}
    <script src="/assets/admin/js/tasks/show.js"></script>
@stop

@section('content')
    <div class="main-content page-profile">
        <div class="page-header">
            <h4 class="page-title">{{$info->title}}</h4>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="card">
                                    <div class="card-header"><h6>Информация</h6></div>
                                    <div class="card-body">
                                        <h5 class="section-semi-title">Задача</h5>
                                        <p>{!! $info->description !!}</p>
                                        <h5 class="section-semi-title">Сроки</h5>
                                        <p>{{date('d.m.Y',strtotime($info->date_start))}} - {{date('d.m.Y',strtotime($info->date_end))}}</p>
                                        <input type="hidden" id="task_start_date" value="{{date('d.m.Y',strtotime($info->date_start))}}">
                                        <input type="hidden" id="task_end_date" value="{{date('d.m.Y',strtotime($info->date_end))}}">
                                        <h5 class="section-semi-title">Статус</h5>
                                        <p>{{$info->status->title}}</p>
                                        <h5 class="section-semi-title">Проект</h5>
                                        <p>{{$project_info->title}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="card">
                                    <div class="card-header"><h6>Комментарии</h6></div>
                                    <div class="card-body">
                                        <div id="comments" data-href="{{route('admin.tasks.comment.get', $info->id)}}">
                                            
                                        </div>
                                        <form action="{{route('admin.tasks.comment.add')}}" id="form-add-comment" novalidate>
                                            <input type="hidden" name="task_id" value="{{$info->id}}">
                                            <textarea class="ls-summernote" id="comment" name="comment"></textarea>
                                            <div class="text-center" style="margin-top:10px;">
                                                <button type="button" class="btn btn-success" data-add-comment>Добавить</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="card">
                                    <div class="card-header"><h6>Участники</h6></div>
                                    <div class="card-body">
                                        <h5 class="section-semi-title">Добавил</h5>
                                        <p>{{$info->user->name}}</p>
                                        <h5 class="section-semi-title">Клиент</h5>
                                        <p>{{$info->project->client->name}}</p>
                                        <h5 class="section-semi-title">Менеджер</h5>
                                        <p>{{$info->project->manager->name}}</p>
                                        <h5 class="section-semi-title">Исполнители</h5>
                                        @foreach($info->task_users as $worker)
                                            <p>{{$worker->user->name}}</p>
                                        @endforeach

                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
