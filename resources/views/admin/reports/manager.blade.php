@extends('admin.layouts.layout-horizontal')

@section('scripts')
    {{--<script src="/assets/admin/js/tasks/list.js"></script>--}}
    <script src="/assets/admin/js/reports/manager.js"></script>
@stop

@section('content')
    <div class="main-content page-profile">
        <div class="page-header">
            <h4 class="page-title">Отчет менеджера {{$user->name}}</h4>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Название</th>
                                    <th>Статистика</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><b>Количество проектов в работе</b></td>
                                    <td><b>{{$stat['projects_in_work']}}</b></td>
                                </tr>
                                <tr>
                                    <td><b>Количество задач в работе</b></td>
                                    <td><b>{{$stat['tasks_in_work']}}</b></td>
                                </tr>
                                <tr>
                                    <td><b>Количество клиентов в работе</b></td>
                                    <td><b>{{$stat['clients_in_work']}}</b></td>
                                </tr>
                                <tr>
                                    <td><b>Просроченные задачи</b></td>
                                    <td><b>{{count($stat['tasks_in_end'])}}</b></td>
                                </tr>
                                @if(count($stat['tasks_in_end']) > 0)
                                    <tr>
                                        <td colspan="2">
                                            <table class="table table-sm">
                                                <thead>
                                                    <tr>
                                                        <th>Название</th>
                                                        <th>Проект</th>
                                                        <th>Статус</th>
                                                        <th>Дата начала</th>
                                                        <th>Дата окончания</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                     @foreach($stat['tasks_in_end'] as $task)
                                                        <td><a href="{{route('admin.tasks.show',$task->id)}}">{{$task->title}}</a></td>
                                                        <td><a href="{{route('admin.projects.show',$task->project_id)}}">{{$task->project->title}}</a></td>
                                                        <td>{{$task->status->title}}</td>
                                                        <td>{{date('d.m.Y', strtotime($task->date_start))}}</td>
                                                        <td>{{date('d.m.Y', strtotime($task->date_end))}}</td>
                                                     @endforeach
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                @endif
                                <tr>
                                    <td><b>Просроченные проекты</b></td>
                                    <td><b>{{count($stat['projects_in_end'])}}</b></td>
                                </tr>
                                @if(count($stat['projects_in_end']) > 0)
                                    <tr>
                                        <td colspan="2">
                                            <table class="table table-sm">
                                                <thead>
                                                    <tr>
                                                        <th>Проект</th>
                                                        <th>Статус</th>
                                                        <th>Дата начала</th>
                                                        <th>Дата окончания</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                     @foreach($stat['projects_in_end'] as $project)
                                                        <td><a href="{{route('admin.projects.show',$project->id)}}">{{$task->project->title}}</a></td>
                                                        <td>{{$project->status->title}}</td>
                                                        <td>{{date('d.m.Y', strtotime($project->date_start))}}</td>
                                                        <td>{{date('d.m.Y', strtotime($project->date_end))}}</td>
                                                     @endforeach
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style type="text/css">
        .table-sm a{
            color: #007dcc;
            text-decoration: underline;
        }
    </style>
@stop
