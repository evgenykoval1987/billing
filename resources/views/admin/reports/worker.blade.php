@extends('admin.layouts.layout-horizontal')

@section('scripts')
    {{--<script src="/assets/admin/js/tasks/list.js"></script>--}}
    <script src="/assets/admin/js/reports/manager.js"></script>
@stop

@section('content')
    <div class="main-content page-profile">
        <div class="page-header">
            <h4 class="page-title">Отчет исполнителя {{$user->name}}</h4>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Название</th>
                                    <th>Статистика</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><b>Количество задач в работе</b></td>
                                    <td><b>{{$stat['tasks_in_work']}}</b></td>
                                </tr>
                                <tr>
                                    <td><b>Количество просроченных задач в работе</b></td>
                                    <td><b>{{$stat['tasks_in_end']}}</b></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style type="text/css">
        .table-sm a{
            color: #007dcc;
            text-decoration: underline;
        }
    </style>
@stop
