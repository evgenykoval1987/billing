<form action="{{route('login.post')}}" id="loginForm" method="post">
    {{csrf_field()}}
    <div class="form-group">
        <input type="email" class="form-control" name="email" placeholder="E-mail">
    </div>
    <div class="form-group">
        <input type="password" class="form-control" name="password" placeholder="Пароль">
    </div>
    <button class="btn btn-theme btn-full">Войти</button>
</form>