var Calendar = (function () {
  var cl = false;
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  var handleCalendar = function () {
    cl = $('#calendar').fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
      },
      //defaultDate: '2017-08-12',
      navLinks: true, // can click day/week names to navigate views
      businessHours: true, // display business hours
      editable: true,
      locale: 'ru',
      eventClick: function(event) {
        if (event.url) {
          document.location.href = event.url;
          return false;
        }
        if (event.popup) {
          $("#event-view .modal-title").text(event.title);
          $("#event-view .description").text(event.description);
          $("#event-view .dt").text(event.dt);
          $("#event-view .type").text(event.status);
          //document.location.href = event.url;
          $("#event-view").modal('show');
          return false;
        }
      },
      monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль','Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
      events: '/admin/calendar/ajax',
    })
  }

  var handleValidation = function () {
    var form = $('#event-add-form')

    form.validate({
      errorElement: 'span',
      errorClass: 'help-block help-block-error',
      focusInvalid: false,
      ignore: '', 
      rules: {
        title: {
          minlength: 3,
          required: true
        },
        dt: {
          required: true
        },
        status_id: {
          required: true
        }
      },

      highlight: function (element) {
        $(element)
        .closest('.form-group .form-control').addClass('is-invalid')
      },
      unhighlight: function (element) {
        $(element)
        .closest('.form-group .form-control').removeClass('is-invalid')
        .closest('.form-group .form-control').addClass('is-valid')
      },
      errorPlacement: function (error, element) {
        if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
          error.insertAfter(element.parent())
        } else {
          error.insertAfter(element)
        }
      },
      success: function (label) {
        label
        .closest('.form-group .form-control').removeClass('is-invalid')
      },
      submitHandler: function(){
        
      }
    });
  }

  $("[data-event-add-button]").click(function(){
    if ($("#event-add-form").valid()){
      
      $.ajax({
        type: 'POST',
        data: $("#event-add-form input, #event-add-form textarea, #event-add-form select"),
        url: $("#event-add-form").attr('action'),
        success: function (data) { 
          toastr['success']('Событие добавлено', 'Success');
          //handleCalendar();
          $('#calendar').fullCalendar('refetchEvents');
          $('.modal').modal('hide');
        },
        error: function (data) { 
          toastr['error']('Ошибка', 'Error')
        }
      })
      return false;
    }
  })

  return {
    // main function to initiate the module
    init: function () {
      handleCalendar()
      handleValidation()
    }
  }
})()

jQuery(document).ready(function () {
  Calendar.init()


})
