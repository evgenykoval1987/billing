var Project = (function () {
	var form = $("[data-project-form]");
  	$.ajaxSetup({
      	headers: {
          	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      	}
  	});

  	var Validation = function () { 
	    form.validate({
	      	errorElement: 'span',
	      	errorClass: 'help-block help-block-error',
	      	focusInvalid: false,
	      	ignore: '', 
	      	rules: {
		        client_id: {
		          	required: true
		        },
		        manager_id: {
		          	required: true
		        },
		        name: {
		          	required: true
		        },
		        status_id: {
		          	required: true
		        },
		        date_start: {
		          	required: true
		        },
		        date_end: {
		          	required: true
		        },
	      	},

	      	highlight: function (element) {
	        	$(element).closest('.form-group .form-control').addClass('is-invalid')
	      	},
	      	unhighlight: function (element) {
	        	$(element).closest('.form-group .form-control').removeClass('is-invalid').closest('.form-group .form-control').addClass('is-valid')
	      	},
	      	errorPlacement: function (error, element) {
		        if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
		          	error.insertAfter(element.parent())
		        } else {
		          	error.insertAfter(element)
		        }
	      	},
	      	success: function (label) {
	        	label.closest('.form-group .form-control').removeClass('is-invalid');
	        	/*if (form.valid())
	        		form.submit();*/
	      	},
	      	submitHandler: function(){
	      		//alert();
	      		form.destroy();
	      		//alert(form.valid());
	        	form.submit();
	        	//return false;
	      	}
	    });

	}

  	return {
	    init: function () {
	      	Validation()
	    }
  	}
})()

jQuery(document).ready(function () {
  	Project.init()
})