var Comment = (function () {
	var form = $("#form-add-comment")
	var adding = function(){
		$("[data-add-comment]").click(function(){ 
		    if (form.valid()){
		      	$.ajax({
			        type: 'POST',
			        data: $("#form-add-comment input, #form-add-comment textarea"),
			        url: form.attr('action'),
			        success: function (data) { 
			          	toastr['success']('Комментарий добавлен', 'Success');
			          	$("#form-add-comment textarea").empty();
			          	$('.ls-summernote').summernote("reset");
			          	updateComments();
			        },
			        error: function (data) { 
			          	toastr['error']('Ошибка', 'Error')
			        }
		      	})
		      	return false;
		    }
		})

		updateComments();
		
	}

	var updateComments = function(){
		$("#comments").load($("#comments").data('href'));
	}

	var handleValidation = function () {
	    form.validate({
	      	errorElement: 'span',
	      	errorClass: 'help-block help-block-error',
	      	focusInvalid: false,
	      	ignore: '', 
	      	rules: {
		        comment: {
		          	required: true
		        }
	      	},

	      	highlight: function (element) {
		        $(element).closest('.form-group .form-control').addClass('is-invalid')
		    },
	      	unhighlight: function (element) {
	        	$(element).closest('.form-group .form-control').removeClass('is-invalid').closest('.form-group .form-control').addClass('is-valid')
	      	},
	      	errorPlacement: function (error, element) {
		        if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
		          	error.insertAfter(element.parent())
		        } else {
		          	error.insertAfter(element)
		        }
	      	},
	      	success: function (label) {
	        	label.closest('.form-group .form-control').removeClass('is-invalid')
	      	},
	      	submitHandler: function(){
	        	//alert();
	      	}
	    });
  	}
	return {
	    init: function () {
	      	adding()
	      	handleValidation()
	    }
	}
})()

jQuery(document).ready(function () {
  Comment.init()
})