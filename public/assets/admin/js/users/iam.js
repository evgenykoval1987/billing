var Users = (function () {
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  
  var editEvents = function(){

    if ($('[data-edit-role-id]').val() == 4)
      $('[data-edit-company]').show();
    else
      $('[data-edit-company]').hide();

    var form = $('#user-iam-form')

    form.validate({
      errorElement: 'span',
      errorClass: 'help-block help-block-error',
      focusInvalid: false,
      ignore: '', 
      rules: {
        password_confirmation: {
          equalTo: '#epassword'
        }
      },

      highlight: function (element) {
        $(element)
        .closest('.form-group .form-control').addClass('is-invalid')
      },
      unhighlight: function (element) {
        $(element)
        .closest('.form-group .form-control').removeClass('is-invalid')
        .closest('.form-group .form-control').addClass('is-valid')
      },
      errorPlacement: function (error, element) {
        if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
          error.insertAfter(element.parent())
        } else {
          error.insertAfter(element)
        }
      },
      success: function (label) {
        label
        .closest('.form-group .form-control').removeClass('is-invalid')
      }
    });
  }

  $("[data-user-edit-button]").click(function(){ 
    if ($("#user-iam-form").valid()){ 
      $.ajax({
        type: 'POST',
        data: $("#user-iam-form input"),
        url: $("#user-iam-form").attr('action'),
        success: function (data) { 
          toastr['success']('Данные сохранены', 'Success');
        },
        error: function (data) { 
          toastr['error']('Ошибка', 'Error')
        }
      })
      return false;
    }

    return false;
  })


  return {
    // main function to initiate the module
    init: function () {
      editEvents()
    }
  }
})()

jQuery(document).ready(function () {
  Users.init()
})
