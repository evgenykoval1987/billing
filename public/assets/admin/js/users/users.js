var Users = (function () {
  var table = false;
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  var handleTables = function () {
    table = $('#users-datatable').DataTable({
      ajax: {
        url: "users/ajax"
      },
      order: [[ 3, "desc" ]],
      responsive: true,
      "language": {
        "processing": "Подождите...",
        "search": "Поиск:",
        "lengthMenu": "Показать _MENU_ записей",
        "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
        "infoEmpty": "Записи с 0 до 0 из 0 записей",
        "infoFiltered": "(отфильтровано из _MAX_ записей)",
        "infoPostFix": "",
        "loadingRecords": "Загрузка записей...",
        "zeroRecords": "Записи отсутствуют.",
        "emptyTable": "В таблице отсутствуют данные",
        "paginate": {
          "first": "<",
          "previous": "<<",
          "next": ">>",
          "last": ">"
        },
        "aria": {
          "sortAscending": ": активировать для сортировки столбца по возрастанию",
          "sortDescending": ": активировать для сортировки столбца по убыванию"
        }
      }
    })

    table.on( 'draw', function () {
      handleConfirmation();
    });
  }

  var editEvents = function(){
    $('[data-edit-role-id]').on('change', function(){
      if ($(this).val() == 4)
        $('[data-edit-company]').show();
      else
        $('[data-edit-company]').hide();
    })

    if ($('[data-edit-role-id]').val() == 4)
      $('[data-edit-company]').show();
    else
      $('[data-edit-company]').hide();

    var form = $('#user-edit-form')

    form.validate({
      errorElement: 'span',
      errorClass: 'help-block help-block-error',
      focusInvalid: false,
      ignore: '', 
      rules: {
        password_confirmation: {
          equalTo: '#epassword'
        },
        email: {
          required: true,
          email: true
        },
        role_id: {
          required: true
        },
      },

      highlight: function (element) {
        $(element)
        .closest('.form-group .form-control').addClass('is-invalid')
      },
      unhighlight: function (element) {
        $(element)
        .closest('.form-group .form-control').removeClass('is-invalid')
        .closest('.form-group .form-control').addClass('is-valid')
      },
      errorPlacement: function (error, element) {
        if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
          error.insertAfter(element.parent())
        } else {
          error.insertAfter(element)
        }
      },
      success: function (label) {
        label
        .closest('.form-group .form-control').removeClass('is-invalid')
      }
    });
  }

  var handleConfirmation = function () {
    $('[data-confirmation="notie"]').on('click', function () {
      $this = $(this)
      notie.confirm({
        text:'Вы уверены?', 
        submitCallback: function () {
          deleteUser($this)
        },
        submitText: 'Да', 
        cancelText: 'Отмена'
      });

      return false
    })

    $('[data-lock]').on('click', function () {
      $this = $(this)
      lockUser($this);
      return false
    })
    $('[data-unlock]').on('click', function () {
      $this = $(this)
      unlockUser($this);
      return false
    })
    $('[data-role-id]').on('change', function(){
      if ($(this).val() == 4)
        $('[data-company]').show();
      else
        $('[data-company]').hide();
    })

    if ($('[data-role-id]').val() == 4)
      $('[data-company]').show();
    else
      $('[data-company]').hide();

    $('[data-edit]').on('click', function () {
      $this = $(this)
      editUser($this);
      return false
    })
  }

  var handleValidation = function () {
    var form = $('#user-add-form')

    form.validate({
      errorElement: 'span',
      errorClass: 'help-block help-block-error',
      focusInvalid: false,
      ignore: '', 
      rules: {
        password: {
          minlength: 6,
          required: true
        },
        password_confirmation: {
          equalTo: '#password',
          required: true
        },
        email: {
          required: true,
          email: true,
          remote: "/admin/users/check-email"
        },
        role_id: {
          required: true
        },
      },

      highlight: function (element) {
        $(element)
        .closest('.form-group .form-control').addClass('is-invalid')
      },
      unhighlight: function (element) {
        $(element)
        .closest('.form-group .form-control').removeClass('is-invalid')
        .closest('.form-group .form-control').addClass('is-valid')
      },
      errorPlacement: function (error, element) {
        if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
          error.insertAfter(element.parent())
        } else {
          error.insertAfter(element)
        }
      },
      success: function (label) {
        label
        .closest('.form-group .form-control').removeClass('is-invalid')
      },
      submitHandler: function(){
        alert();
      }
    });
  }

  $("[data-user-add-button]").click(function(){
    if ($("#user-add-form").valid()){
      $.ajax({
        type: 'POST',
        data: $("#user-add-form input, #user-add-form select"),
        url: $("#user-add-form").attr('action'),
        success: function (data) { 
          toastr['success']('Пользователь добавлен', 'Success');
          table.ajax.reload();
          $('.modal').modal('hide');
        },
        error: function (data) { 
          toastr['error']('Ошибка', 'Error')
        }
      })
      return false;
    }
  })

  $("[data-user-edit-button]").click(function(){
    if ($("#user-edit-form").valid()){
      $.ajax({
        type: 'POST',
        data: $("#user-edit-form input, #user-edit-form select"),
        url: $("#user-edit-form").attr('action'),
        success: function (data) { 
          toastr['success']('Пользователь изминен', 'Success');
          table.ajax.reload();
          $('.modal').modal('hide');
        },
        error: function (data) { 
          toastr['error']('Ошибка', 'Error')
        }
      })
      return false;
    }
  })

  var lockUser = function(){
    var url = $this.attr('href')
    var token = $this.data('token')
    console.log(url);
    $.ajax({
      type: 'POST',
      data: {_token: token},
      url: url,
      success: function (data) {
        toastr['success']('Пользователь заблокирован', 'Success');
        table.ajax.reload();
        //Window.setTimeout('location.reload()', 500)
      },
      error: function (data) {
        toastr['error']('Ошибка', 'Error')
      }
    })
  }

  var unlockUser = function(){
    var url = $this.attr('href')
    var token = $this.data('token')
    console.log(url);
    $.ajax({
      type: 'POST',
      data: {_token: token},
      url: url,
      success: function (data) {
        toastr['success']('Пользователь разблокирован', 'Success');
        table.ajax.reload();
        //Window.setTimeout('location.reload()', 500)
      },
      error: function (data) {
        toastr['error']('Ошибка', 'Error')
      }
    })
  }

  var deleteUser = function ($this) {
    var url = $this.attr('href')
    var token = $this.data('token')
    console.log(url)
    $.ajax({
      type: 'POST',
      data: {_method: 'delete', _token: token},
      url: url,
      success: function (data) {
        toastr['success']('Пользователь удален', 'Success')
        table.ajax.reload();
      },
      error: function (data) {
        toastr['error']('Ошибка', 'Error')
      }
    })
  }

  var editUser = function(){
    var url = $this.attr('href')
    $.ajax({
      type: 'GET',
      url: url,
      success: function (data) {
        $("#user-edit .modal-body").empty().append(data);
        $("#user-edit").modal('show');
        editEvents();
      },
      error: function (data) {
        toastr['error']('Ошибка', 'Error')
      }
    })
  }

  return {
    // main function to initiate the module
    init: function () {
      handleTables()
      handleConfirmation()
      handleValidation()
    }
  }
})()

jQuery(document).ready(function () {
  Users.init()
})
