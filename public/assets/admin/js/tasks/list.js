var Tasks = (function () {
  var form = $('#task-add-form');
  var table = false;

  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  var handleValidation = function () {
    form.validate({
      errorElement: 'span',
      errorClass: 'help-block help-block-error',
      focusInvalid: false,
      ignore: '', 
      rules: {
        status_id: {
          required: true
        },
        name: {
          required: true
        },
        date_start: {
          required: true
        }
      },

      highlight: function (element) {
        $(element)
        .closest('.form-group .form-control').addClass('is-invalid')
      },
      unhighlight: function (element) {
        $(element)
        .closest('.form-group .form-control').removeClass('is-invalid')
        .closest('.form-group .form-control').addClass('is-valid')
      },
      errorPlacement: function (error, element) {
        if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
          error.insertAfter(element.parent())
        } else {
          error.insertAfter(element)
        }
      },
      success: function (label) {
        label
        .closest('.form-group .form-control').removeClass('is-invalid')
      },
      submitHandler: function(){
        //alert();
      }
    });
  }

  var handlePlugins = function(){
    $('.lsd-datepicker').datepicker({
      format: 'dd.mm.yyyy',
      language: 'ru',
      startDate: $("#project_start_date").val(),
      endDate: $("#project_end_date").val()
    });
  }

  var handleTables = function () {
    table = $('#tasks-datatable').DataTable({
      ajax: {
        url: "/admin/tasks/ajax",
        data: {
          project_id: $("#project_id").val()
        }
      },
      ordering: true,
      order: [[ 0, "asc" ]],
      responsive: true,
      language: datatavlelang
    })

    table.on( 'draw', function () {
      handleConfirmation();
    });
  }

  var handleConfirmation = function () {
    $('[data-confirmation="notie"]').on('click', function () {
      $this = $(this)
      notie.confirm({
        text:'Вы уверены?', 
        submitCallback: function () {
          deleteTask($this)
        },
        submitText: 'Да', 
        cancelText: 'Отмена'
      });

      return false
    })
  }

  $("[data-task-add-button]").click(function(){
    if (form.valid()){
      $.ajax({
        type: 'POST',
        data: $("#task-add-form input, #task-add-form select, #task-add-form textarea"),
        url: form.attr('action'),
        success: function (data) { 
          toastr['success']('Задача добавлена', 'Success');
          table.ajax.reload();
          $('.modal').modal('hide');
        },
        error: function (data) { 
          toastr['error']('Ошибка', 'Error')
        }
      })
      return false;
    }
  })

  $("[data-task-edit-button]").click(function(){
    if ($("#task-edit-form").valid()){
      $.ajax({
        type: 'POST',
        data: $("#task-edit-form input, #task-edit-form select, #task-edit-form textarea"),
        url: $("#task-edit-form").attr('action'),
        success: function (data) { 
          toastr['success']('Задача изменена', 'Success');
          table.ajax.reload();
          $('.modal').modal('hide');
        },
        error: function (data) { 
          toastr['error']('Ошибка', 'Error')
        }
      })
      return false;
    }
  })

  var deleteTask = function ($this) {
    var url = $this.attr('href')
    var token = $this.data('token')
    console.log(url)
    $.ajax({
      type: 'POST',
      //data: {_method: 'delete', _token: token},
      url: url,
      success: function (data) {
        toastr['success']('Задача удалена', 'Success')
        table.ajax.reload();
      },
      error: function (data) {
        toastr['error']('Ошибка', 'Error')
      }
    })
  }

  $(document).on('click', '[data-edit]', function () {
    $this = $(this)
    editTask($this);
    return false
  })

  var editTask = function(){
    var url = $this.attr('href')
    $.ajax({
      type: 'GET',
      url: url,
      success: function (data) {
        $("#task-edit .modal-body").empty().append(data);
        $("#task-edit").modal('show');
        editEvents();
      },
      error: function (data) {
        toastr['error']('Ошибка', 'Error')
      }
    })
  }

  var editEvents = function(){
    $('.ls-summernote').summernote();
    handlePlugins();
    $('.ls-multi-select').multiSelect();
    handleValidationEdit()
  }

  var handleValidationEdit = function () {
    $('#task-edit-form').validate({
      errorElement: 'span',
      errorClass: 'help-block help-block-error',
      focusInvalid: false,
      ignore: '', 
      rules: {
        status_id: {
          required: true
        },
        name: {
          required: true
        },
        date_start: {
          required: true
        }
      },

      highlight: function (element) {
        $(element)
        .closest('.form-group .form-control').addClass('is-invalid')
      },
      unhighlight: function (element) {
        $(element)
        .closest('.form-group .form-control').removeClass('is-invalid')
        .closest('.form-group .form-control').addClass('is-valid')
      },
      errorPlacement: function (error, element) {
        if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
          error.insertAfter(element.parent())
        } else {
          error.insertAfter(element)
        }
      },
      success: function (label) {
        label
        .closest('.form-group .form-control').removeClass('is-invalid')
      },
      submitHandler: function(){
        //alert();
      }
    });
  }

  return {
    init: function () {
      handleValidation()
      handlePlugins()
      handleTables()
      handleConfirmation()
    }
  }
})()

jQuery(document).ready(function () {
  Tasks.init()
})
