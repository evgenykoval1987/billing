var Projects = (function () {
  var table = false;
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  var handleTables = function () {
    table = $('#projects-datatable').DataTable({
      ajax: {
        url: "/admin/projects/ajax-inwork"
      },
      //order: [[ 3, "desc" ]],
      ordering: false,
      responsive: true,
      language: datatavlelang
    })

    table.on( 'draw', function () {
      //handleConfirmation();
    });
  }

  /*var handleConfirmation = function () {
    $('[data-confirmation="notie"]').on('click', function () {
      $this = $(this)
      notie.confirm({
        text:'Вы уверены?', 
        submitCallback: function () {
          deleteProject($this)
        },
        submitText: 'Да', 
        cancelText: 'Отмена'
      });

      return false
    })
  }

  var deleteProject = function ($this) {
    var url = $this.attr('href')
    var token = $this.data('token')
    console.log(url)
    $.ajax({
      type: 'POST',
      //data: {_method: 'delete', _token: token},
      url: url,
      success: function (data) {
        toastr['success']('Пользователь удален', 'Success')
        table.ajax.reload();
      },
      error: function (data) {
        toastr['error']('Ошибка', 'Error')
      }
    })
  }*/

  return {
    init: function () {
      handleTables()
      //handleConfirmation()
    }
  }
})()

jQuery(document).ready(function () {
  Projects.init()
})
